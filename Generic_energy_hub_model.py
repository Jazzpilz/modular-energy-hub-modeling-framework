"""
This model should represent the described model in ./data/empa_generic_energy_hub/Documentation_model.pdf

Notes
-----
- At the moment it is not yet working (probably due to infeasibility)

@author: Frederik Banis
"""

from interface import outputlib, logger, helpers, logging, solph, Flow, Registry
from interface.hub import Hub, CopyHub
from interface.components import Bus, Conv, Sink, Store
from interface.model import CustomModel
from interface.miscellaneous import show_tree, Size

import pyomo.core as po
import os
import pandas as pd

# ##########################################################################
# Read input data
# ##########################################################################

# We read the last row in the demand data, representing the total demand. We
# assign a proper name to this pd.Series object
demand = pd.read_excel(os.path.join(os.getcwd(), "data", "empa_generic_energy_hub", "Demand.xls"))
demand_total = demand.iloc[-1, :]
demand_total.name = "Total values"
print(demand_total)

# We read hourly values for the demand over all columns in the data
demand = demand.iloc[:8760, :]
print(demand.head())

# as well as production time series. An error caused by numpy yields to the
# task of converting all numbers to floats in order to resolve it
solar = pd.read_excel(os.path.join(os.getcwd(), "data", "empa_generic_energy_hub", "Data.xls"))
solar = solar.iloc[:8760, :]
solar = solar.iloc[:, 0].apply(lambda x: float(x))
print(solar.head())

# Test for valid data lengths:
if len(demand) != len(solar):
    raise ValueError("Fix data lengths")

# ##########################################################################
# Initialize model and components
# ##########################################################################

# We initialise a new model, providing either the whole period length or in
# this case, for test purposes, a reduced number of timesteps for one week:
model = CustomModel(horizon=pd.date_range('1/1/2012', periods=24 * 7, freq='H'))

# We initialise a new hub
h = Hub("Hub")
# as well as busses to carry our energy flows. This is a good place to tweak
# the parameters of the problem when we face infeasibility. Of course in the
# final version of this model, an electric bus should be balanced and not allow
# for excess energy flows, except we want to include some storage behaviour for
# this component:
bel = Bus("Bel", type="Electricity", excess=False, balanced=True)
bgas = Bus("Bgas", type="Gas", excess=True, balanced=False)
bheat = Bus("Bheat", type="Heat", excess=True, balanced=False)

# We need to model two additional energy storages in order to model the energy grids. In addition to the specified
# flows, we need to provide parameters that describe the storage behaviour. We set the initial capacity to the total
# demand, in order to make sure these components are able to cover the full load theoretically.
rel = Store("RESOURCE_EL",
            inp={bel: Flow(variable_costs=-0.14)},
            out={bel: Flow(variable_costs=0.24,
                           co2_var=0.137)},
            nominal_capacity=10 * demand_total["EL"],
            capacity_loss=0,
            nominal_input_capacity_ratio=1,
            nominal_output_capacity_ratio=1,
            inflow_conversion_factor=1,
            outflow_conversion_factor=1,
            initial_capacity=demand_total["EL"],
            capacity_min=0)
rgas = Store("RESOURCE_GAS",
             inp=bgas,
             out={bgas: Flow(variable_costs=0.09 * 60,
                             co2_var=0.198)},
             nominal_capacity=10 * demand_total["HEAT"],
             capacity_loss=0,
             nominal_input_capacity_ratio=1,
             nominal_output_capacity_ratio=1,
             inflow_conversion_factor=1,
             outflow_conversion_factor=1,
             initial_capacity=demand_total["HEAT"],
             capacity_min=0)

# We model the heat pump as a transformer object. As OMV costs are kWh, we divide those
# by 60. We mark this as an investment object by assigning a new instance of solph.Investment()
# as a 'investment' parameter.
hp = Conv("HEATPUMP",
          inp=bel,
          out={bheat: Flow(nominal_value=100,
                           variable_costs=1000+ 0.1/60)},
          conversion_factors={bheat: 3.2},
          lifetime=20,
          investment=solph.Investment(maximum=100))

# We model the boiler as a transformer object, similarly to the heat pump above:
boiler = Conv("BOILER",
              inp=bgas,
              out={bheat: Flow(nominal_value=100,
                               variable_costs=200 + 0.01 / 60)},
              conversion_factors={bheat: 0.94},
              lifetime=30,
              investment=solph.Investment(maximum=100))

# As we have three combined heat-power plants in the problem, we can use the 'Size' class to bundle their parameter
# definition in one place in code. As the minimum load if 50%, be specify the parameter 'min' as 0.5. Missing in this
# component definition is the heat-power ratio, obviously crucial when modelling a chp plant.
# FIXME: Add HPR
chp_size = Size(inp=bgas,
                out={bel: Flow(nominal_value=50,
                               variable_costs=1500 + 0.021 / 60,
                               min=0.5,
                               max=1),
                     bheat: Flow(nominal_value=50,
                                 variable_costs=1500 + 0.021 / 60,
                                 min=0.5,
                                 max=1)},
                # FIXME: Enable determined by HPR
                conversion_factors={bel: 0.15, bheat: 0.15},
                lifetime=20)
chp_1 = Conv("CHP", size=chp_size, investment=solph.Investment(maximum=50/3))
chp_2 = Conv("CHP", size=chp_size, investment=solph.Investment(maximum=50/3))
chp_3 = Conv("CHP", size=chp_size, investment=solph.Investment(maximum=50/3))

# We add the two solar collectors and mark both Flow objects as investment objects.
# As within the stated problem it is not defined whether we will actually use both
# technologies, we specify both as investment objects as well to make the solver
# decide whether they should be added.
# FIXME: Add capital costs per area
pv = Conv("PV",
          inp="Rad",
          out={bel: Flow(actual_value=solar.values,
                         nominal_value=1,
                         eta=0.14,
                         fixed=True,
                         variable_costs=3500 + 0.06 / 60,
                         investment=solph.Investment())},
          lifetime=20,
          investment=solph.Investment())

# FIXME: Add capital costs per area
st = Conv("ST",
          inp="Rad",
          out={bheat: Flow(actual_value=solar.values,
                           eta=0.46,
                           fixed=True,
                           variable_costs=2900 + 0.12 / 60,
                           investment=solph.Investment())},
          lifetime=35,
          investment=solph.Investment())

battery = Store("BATTERY",
                inp={bel: Flow(nominal_input_capacity_ratio=0.3 * 60 / 100)},
                out={bel: Flow(variable_costs=100/60,
                               nominal_output_capacity_ratio=0.3 * 60 / 100)},
                capacity_min=0.3,
                capacity_max=100,
                capacity_loss=0.001,
                initial_capacity=0,
                inflow_conversion_factor=0.9,
                outflow_conversion_factor=0.9,
                lifetime=20,
                investment=solph.Investment(maximum=100))

hw_tank = Store("HOT WATER",
                inp={bheat: Flow(nominal_input_capacity_ratio=0.25 * 60 / 100)},
                out={bheat: Flow(variable_costs=100,
                                 nominal_input_capacity_ratio=0.25 * 60 / 100)},
                capacity_min=0,
                capacity_max=100 * 60,
                capacity_loss=0.01,
                initial_capacity=0,
                inflow_conversion_factor=0.9,
                outflow_conversion_factor=0.9,
                lifetime=17,
                investment=solph.Investment(maximum=100))

# The demand data is already matching the actual values, hence nominal_value is set to one in both sinks
sink_el = Sink("DEMAND_EL",
               inp={bel: Flow(actual_value=demand["EL"].values,
                              fixed=True,
                              nominal_value=1)})
sink_heat = Sink("DEMAND_HEAT",
                 inp={bheat: Flow(actual_value=demand.loc[:, "HEAT"].values,
                                  fixed=True,
                                  nominal_value=1)})

# ##########################################################################
# Add roof area constraints
# ##########################################################################

# We need to call the .prepare method of 'model' in order to start the initial components variable creation. We can then
#  access these variables as shown below.

model.prepare(h)

# Compare 'Example_Constraints' for this part
pv_flows = [(n, [o for o in n.outputs][0])
            for n in h.nodes if 'PV' in n.label]
st_flows = [(n, [o for o in n.outputs][0])
            for n in h.nodes if 'ST' in n.label]
# variable to store the area that is used by pv and sth
model.pv_area = po.Var(within=po.NonNegativeIntegers)
model.st_area = po.Var(within=po.NonNegativeIntegers)

pv_area_per_power = dict(zip(pv_flows, [0.125 * 60] * len(pv_flows)))
st_area_per_power = dict(zip(st_flows, [0.7 * 60] * len(st_flows)))

max_area = 50  # m²

model.pv_area_constr = po.Constraint(expr=model.pv_area == \
                                          sum(model.InvestmentFlow.invest[s, t] * pv_area_per_power[s, t]
                                              for (s, t) in pv_flows))

model.sth_area_constr = po.Constraint(expr=model.st_area == \
                                           sum(model.InvestmentFlow.invest[s, t] * st_area_per_power[s, t]
                                               for (s, t) in st_flows))

model.max_area_usage = po.Constraint(expr=model.pv_area + model.st_area <= max_area)

# model.pprint()  # prints complete optimization model  CAUTION: do this only with few timesteps
h.optimize()
h.plot(save_plot=True, plot_format='pdf', ylabel='kW')
