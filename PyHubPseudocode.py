# TODO rework structure (no doubled naming)
import os

import pandas as pd

from interface.convertor import Conv
from interface.hub import Hub, CopyHub
from interface.miscellaneous import Simulation, Size, connect_hubs, copy_hub, add_constraint, Units, assign_loads
from interface.from_db import ConvFromDB, SelectionFromDB
from components.bus import Bus
from oemof.tools import logger
from oemof.solph import predefined_objectives as predefined_objectives

logger.define_logging()

csv_file = os.path.join(os.getcwd(), 'data', 'storage_invest.csv')
data = pd.read_csv(csv_file, sep=',')
running_costs = {'grid-in': 0.24, 'grid-out': -0.14, 'gas': 0.09}  # cost coefficients
CO2rates = {'grid-in': 0.137, 'grid-out': 0, 'gas': 0.198}  # CO2 coefficients
horizon = pd.date_range('1/1/2012', periods=168, freq='H')

# # The simulation object makes the time horizon available via config.horizon,
# # as well as itself via config.sim
sim = Simulation(solver='cbc', horizon=horizon, verbose=True, debug=True,
                 objective_options={'function': predefined_objectives.minimize_cost})
sim.set_objective()

h = Hub('House')

grid_in = Bus('grid-in', type='el', price=running_costs['grid-in'])
grid_exp = Bus('grid-exp', type='el', price=running_costs['grid-out'])
elec = Bus('electricity', type='el')
heat = Bus('heat', type='heat')

# gridimp = Conv('Grid Import', 1, grid_in, elec)
# gridexp = Conv('Grid Export', 1, elec, grid_exp)

pvsize = Size('PV size', 'integer', ub=10000, cost=437.5)
pv = Conv('PV', 0.14, 'solar', elec, val=data['pv'], size=5*pvsize)

st = Conv('ST', 0.46, 'solar', heat, val=data['pv'],
          size=5*Size('ST size', 'integer', ub=10, cost=362.5))

# TODO add _constraint func
add_constraint(pvsize+st.size <= 10)

# TODO ConvFromDB cls
hp = ConvFromDB('Air source heat pump 22')
boiler = Conv('Boiler',0.94,'gas','heat') # no upper bound so capacity is infinite
# TODO add Units class for unit support
chpunits = Units('CHP units',ub=int(3),cost=1500) # discrete number of units 1-3
# TODO add CHP support
chp = Conv('CHP',[0.3*1.73,0.3],'gas',['heat','elec'],50,mainenance=0.021,minload=0.5,units=chpunits) # inc mainenance costs and minimum load constraints
tanksize = Size('Tank size','continuous',ub=100,cost=100)
# Storage cls mplemented via Convertor
tank = Conv('Hot water tank',0.9,'heat','heat',tanksize,0.01,0.9,0.25*tanksize)
# TODO reorganize SelectionFromDB class to cover selection of sets
bat = SelectionFromDB('Technology general name == Battery', 'Capacity > 10') # select set of possible converters from database
add_constraint(bat.state >= 0.3*bat.size)
# TODO modify oemof in order to be able to create hub objects and assign components after their creation
# h = Hub('House',horizon) # create an empty hub
# h.components = [gridimp,gridexp,pv,st,hp,boiler,chp,tank,bat]
# h.optimize()
# TODO add plot capability
# h.plot()
# print(h.components['PV].outputs)

# gridimp = Conv('District Heating Import',1,'DH-in','heat') # add district heating to hub h
# gridexp = Conv('District Heating Export',1,'heat','DH-out')
district = CopyHub(h,10)

for i, hub in enumerate(district): # for each hub
    hub.loads = assign_loads(data['loads']) # assign new loads
district[2].components['CHP'] = [] # delete chp for hub 2 (by index)
district['House 3'].gshp = ConvFromDB('Ground source heat pump') # add GSHP to House 3 (by name)

DHtank = Conv('District-scale hot water tank',0.9,'DH','DH',1000,0.01,0.9,0.25) # add a centralised storage (becomes district[11])
districtpvsize = Size('PV size','integer',ub=int(10),cost=437.5) # add a district-level sizing variable (same for all houses)
for i, hub in enumerate(district): # for each hub
    hub.pvsize = districtpvsize # overwrite building-level sizing

# AddNetwork('DH',loss=0.05) # add network constraints for type DH with heat loss of 5% per km
# district.networklinks = [(1,2),(2,3),(2,4),(4,5),(4,6),(4,7),(4,11),(1,8),(8,9),(9,10)] # add links according to a list of in-out pairs
# district.networklengths = [10,5,20,30,10,10,10,2,50,5,5] # assign lengths to links

# district.optimise()

###### Connect 3 districts together
# city = CopyHub(district,3)
#
# city.networklinks = [(1[11],2[11]), (2[11],3[11]), (1[11],3[11])] # make a ring network between node 11 of each district
# city.networklengths = [200, 250, 200]
#
# city.optimise()