"""
This example shall illustrate how to add additional constraints on top of an existing model

Notes
-----

@author: Frederik Banis (Example provided by Simon Hilpert (simon.hilpert@fh-flensburg.de))
"""

import os

import pandas as pd
import pyomo.core as po

from interface import Registry, logging, logger, solph, Flow
from interface.model import CustomModel, OperationalModel
from interface.components import Bus, Conv, Sink
from interface.hub import Hub

number_timesteps = 168
# ##########################################################################
# # Initialize components of hub House
# ##########################################################################
logging.info('Initialize the energy system')

model = CustomModel(horizon=pd.date_range('1/1/2012', periods=number_timesteps, freq='H'),
                    solver="glpk", tee=True)

# Create energy system
hub = Hub('Hub')

# Read data file
csv_file = os.path.join(os.getcwd(), 'data', 'storage_invest_long.csv')
data = pd.read_csv(csv_file, sep=',')

##########################################################################
# Create oemof objects
##########################################################################

logging.info('Create oemof objects')

bgas = Bus(label='natural_gas')

bel = Bus(label='electricity')

bheat = Bus(label="heat")

Sink(label="excess_bel", inp=bel)

# create commodity object for gas resource
Conv(label='rgas', out={bgas: Flow(
    nominal_value=194397000, summed_max=1)})

wind = Conv(label='wind',
            inp='wind',
            out={bel: Flow(actual_value=data["wind"],
                           nominal_value=1000000,
                           fixed=True,
                           fixed_costs=20)})

# -----------------------------------------------------
# Adding the possibility of having some PV and solar thermal panel and /
# calculating the best design to reduce operating cost, limiting the size occupied /
# by both of the two panel types for example.

# create fixed source object for pv
# these objects need to be have an invest objects as well (if not the
# constraints below would only check if the area is large enough for the assumed
# installed capacities and return an infeasible model if not...)

annual_costs_pv = 10e3  # €/a
ep_costs_pv = annual_costs_pv * (number_timesteps / 8760)  # per defined period
pv = Conv(label='pv', outputs={bel: Flow(
    actual_value=data['pv'],
    fixed_costs=15, variable_costs=10, fixed=True,
    investment=solph.Investment(ep_costs=ep_costs_pv))})

annual_costs_sth = 11e3  # €/a
ep_costs_sth = annual_costs_sth * (number_timesteps / 8760)  # per defined period
sth = Conv(label="s_th", outputs={bheat: Flow(
    actual_value=data['pv'],
    fixed_costs=25, variable_costs=20,
    fixed=True, investment=solph.Investment(ep_costs=ep_costs_sth))})

# Rule: pv.area + sth.area <= max_area = 100000 m², constraints are added below

# -----------------------------------------------------

Sink(label='demand', inp={bel: Flow(actual_value=data['demand_el'],
                                    fixed=True,
                                    nominal_value=1)})

Conv(label="pp_gas",
     inp=bgas,
     out={bel: Flow(nominal_value=10e10,
                    variable_costs=50)},
     conversion_factors={bel: 0.58})

# Calculate ep_costs from capex to compare with old solph
capex = 1000
lifetime = 20
wacc = 0.05
epc = capex * (wacc * (1 + wacc) ** lifetime) / ((1 + wacc) ** lifetime - 1)

# create storage transformer object for storage
solph.Storage(label='storage',
              inputs={bel: Flow(variable_costs=10e10)},
              outputs={bel: Flow(variable_costs=10e10)},
              capacity_loss=0.00, initial_capacity=0,
              nominal_input_capacity_ratio=1 / 6,
              nominal_output_capacity_ratio=1 / 6,
              inflow_conversion_factor=1, outflow_conversion_factor=0.8,
              fixed_costs=35,
              investment=solph.Investment(ep_costs=epc))

model.prepare(hub)

# ############################## ADDITIONAL CONSTRAINTS ###################

# group all associated flows of pv nodes
pv_flows = [(n, [o for o in n.outputs][0])
            for n in hub.nodes if 'pv' in n.label]
sth_flows = [(n, [o for o in n.outputs][0])
             for n in hub.nodes if 'sth' in n.label]
# variable to store the area that is used by pv and sth
model.pv_area = po.Var()
model.sth_area = po.Var()

# now for every pv object (flow) the same area per power is used, but can be
# altered of course if you have different pv/sth technologies with different area
# demand  (could be a pyomo set but dict is also fine)
pv_area_per_power = dict(zip(pv_flows, [1000] * len(pv_flows)))  # e.g. m2/W depends on unit of installed capacity
sth_area_per_power = dict(zip(sth_flows, [1000] * len(sth_flows)))  # like above...

# total avaiable area
max_area = 100000  # m2


def pv_area_rule(model):
    return (model.pv_area == sum(
        model.InvestmentFlow.invest[s, t] * pv_area_per_power[s, t]
        for (s, t) in pv_flows))


model.pv_area_constr = po.Constraint(rule=pv_area_rule)

# alternative without rule (because there is no 'forall' index just sums over the set)
model.sth_area_constr = po.Constraint(expr=model.sth_area == \
                                           sum(model.InvestmentFlow.invest[s, t] * sth_area_per_power[s, t]
                                               for (s, t) in sth_flows))
model.max_area_usage = po.Constraint(expr=
                                     model.pv_area + model.sth_area <= max_area)

# model.pv_area_constr.pprint()
# model.max_area_usage.pprint()
# model.pprint()  # prints complete optimization model  CAUTION: do this only with few timesteps

hub.optimize()
hub.plot()
