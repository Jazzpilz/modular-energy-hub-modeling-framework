This file should describe what has been done so far.

General principle (and goal)
===========================
Keep it small and simple, being able to add as many attributes to a model as wanted.

Nesting
=============
-  EnergySystem child class: Create Hub object
-  Label issue: Each component gets individual label-> Avoid copying issues. See cls <Registry>
-  Nesting container: Created Copy_hub cls
-  Re-initialisation of components when copying (cloning): implement parameter information (kwargs) dictionary enabling later accessing and thus copying of parameters when copying components. (Should be improved in the future, so that parameters are always up-to-date, even when they have been changed after initialisation of a component).
-  Label_count by type of component (e.g. Bus_10 relates to Bus number 11 in model)
-  CopyHub.networklinks: Specification for links between busses withing a network

Interface to Oemof
========================
-  Interface components: Conv, Sink, Store class
-  Hub.components list and mapping to oemof entities
Works also now by direct subscription access, e.g. hub["electricity"]. (Should be improved so that this not only works one-way, but Oemof containers match Hub.components. Fails atm as Oemof atm does not allow for removal of components as soon as they are attached).
-  Implemented model.set_objective() in order to be able to set the objective function
-  Results data access: Hub.components[key].output (or .input) or Hub.components[key].to_bus (or .from_bus)
-  Combine list and dictionary functionalities
Cp. Collections OrderedDict and SortedContainters:
http://www.grantjenks.com/docs/sortedcontainers/introduction.html

Constraints
===========
-  Size class for sizing of components sharing one source of parameter information: Implemented Size class

Configuring components from databases
=====================================
-  Implemented ConvFromDB class: Configuring converters using a database file
-  Implemented SinkFromDB class: Configuring sinks using a database file
-  Implemented StoreFromDB class: Configuring storages using a database file

Tree and results plotting
=========================
-  Chose GraphViz package for plotting: http://graphviz.org/
-  Parameter input: Implemented necessary parameters for the visualisation
-  .networklinks method of CopyHub links busses in tree plot also
-  Results plotting: Hub.plot(), CopyHub.plot()
-  Allow for output filetype specification
-  Restrict output to a maximum of 10 figures corresponding to a max of 5 busses in a hub. Plotting can be forced providing 'force_show=True'.
-  Use Seaborn for choosing a color palette and styling of the figure (despine, context, style)
-  Figure label specifications
