"""
@author: Frederik Banis
Model provided by: Julien Marquant
"""

# Framework objects and functions
from interface import solph, Flow, logger, logging
from interface.model import CustomModel
from interface.hub import Hub
from interface.components import Bus, Conv, Sink, Store
from interface.miscellaneous import show_tree

# Additional imports
import os
import pandas as pd
import matplotlib.pyplot as plt

# ##########################################################################
# # Initialize data and options
# ##########################################################################

model = CustomModel(horizon=pd.date_range('1/1/2012', periods=288, freq='D'),
                    solver="glpk", tee=True)

logging.info('Initialize the energy system')

# Read data
excel_file = os.path.join(os.getcwd(), 'data', "empa_district_test", 'data_loads_supply.xlsx')

heatloads = pd.read_excel(excel_file, sheetname="heatloads")
heatloads = heatloads.iloc[:288]
print(heatloads)

elecloads = pd.read_excel(excel_file, sheetname="elecloads")
elecloads = elecloads.iloc[:288]
print(elecloads)
elecloads_sum = elecloads.sum().sum()
print(elecloads_sum)

solar = pd.read_excel(excel_file, sheetname="solar")
print(solar)

if len(heatloads) != len(elecloads) != len(solar):
    raise ValueError("Data length mismatch")

plot = elecloads.boxplot(return_type='axes')
plot.set_xlabel("Houses")
plot.set_ylabel("kW")
plt.show()

# ##########################################################################
# # Initialize components
# ##########################################################################
logging.info('Create oemof objects')

hub = Hub()

bel = Bus("electricity", type="el", balanced=True, excess=False)
bgas = Bus("gas", type="gas", balanced=True, excess=False)
gas = Conv("rgas",
           out={bgas: Flow(variable_costs=0.088)})
bheat = Bus("bheat", type="heat", balanced=True, excess=False)

grid = Store("grid",
             inp={bel: Flow(variable_costs=0.088 * 60)},
             out={bel: Flow(variable_costs=0.11 * 60)},
             nominal_capacity=elecloads_sum,
             initial_capacity=elecloads_sum,
             capacity_loss=0,
             nominal_input_capacity_ratio=1,
             nominal_output_capacity_ratio=1,
             inflow_conversion_factor=1,
             outflow_conversion_factor=1)

# FIXME: Adapt
# Calculate ep_costs from capex to compare with old solph
capex = 1000
lifetime = 20
wacc = 0.05
epc = capex * (wacc * (1 + wacc) ** lifetime) / ((1 + wacc) ** lifetime - 1)
Store("sto_th",
      inp={bheat: Flow(variable_costs=40)},
      # FIXME Remove variable costs
      out={bheat: Flow(variable_costs=40)},
      fixed_costs=200,
      capacity_loss=0.00,
      initial_capacity=0, cap_max=150,
      nominal_input_capacity_ratio=0.4,
      nominal_output_capacity_ratio=0.4,
      inflow_conversion_factor=0.99,
      outflow_conversion_factor=0.99,
      investment=solph.Investment(ep_costs=epc))

Conv("boiler",
     inp={bgas: Flow(fixed_costs=200,
                     variable_costs=50)},
     out={bheat: Flow(nominal_value=15,
                      max=1,
                      min=0.13)},
     conversion_factors={bheat: 0.8})

# FIXME: Fixed and variable costs in relation to area
# FIXME: price
Conv("pv",
     out={bel: Flow(fixed_costs=10,
                    variable_costs=0.088,
                    price=0.088,
                    actual_value=solar["output"] * 100,
                    nominal_value=0.2)})

Conv("chp",
     inp={bgas: Flow(fixed_costs=250,
                     variable_costs=100)},
     out=[bel, bheat],
     conversion_factors={bel: 0.25, bheat: 0.5},
     lb=4, ub=500)

for num in range(8):
    Sink("Heat_House_{0}".format(num + 1),
         inp={bheat: Flow(actual_value=heatloads.loc[:, "House_{0}".format(num + 1)].values,
                          nominal_value=1)})

    Sink("El_House_{0}".format(num + 1),
         inp={bel: Flow(actual_value=elecloads.loc[:, "House_{0}".format(num + 1)].values,
                        nominal_value=1)})

model.pprint()  # prints complete optimization model  CAUTION: do this only with few timesteps
hub.optimize()
hub.plot(force_show=True)
