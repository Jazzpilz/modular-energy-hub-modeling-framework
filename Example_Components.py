"""
This example shall illustrate different ways to create components of an energy system

Notes
-----

@author: Frederik Banis
"""

import os
import pandas as pd

from interface import Registry, Flow, solph
from interface.hub import Hub
from interface.components import Bus, Conv, Sink, Store
from interface.model import CustomModel
from interface.from_db import ConvFromDB
from interface.miscellaneous import show_tree

# ##########################################################################
# Read input data
# ##########################################################################

csv_file = os.path.join(os.getcwd(), 'data', 'storage_invest.csv')
data = pd.read_csv(csv_file, sep=',').iloc[:, 1:]

# ##########################################################################
# Initialize model and components
# ##########################################################################

model = CustomModel(horizon=pd.date_range('1/1/2012', periods=len(data), freq='H'),
                    solver='glpk',
                    solve_kwargs={'tee': True})

# All components below are added automatically thanks to Oemofs behaviour in this respect.
# As soon as another hub is created, this behaviour is repeated for the later created one.
hub_1 = Hub()

# We can create a bus object a simple way like below. Providing a type
# specification is helpful.
bel = Bus("Electricity", type="el", balanced=False, excess=True)
bheat = Bus("Heat", type="heat", balanced=True, excess=False)

# We could also create an instance of a component class without assigning it to a
# variable. This could be the case when using a CopyHub container for multiplying
# a hub object or in case we encapsulate the process of component creation for readability
# and solely return a hub object.
Bus("Grid", type="el", price=50)

# Generally we can specify all components as we could do in Oemof, however we
# can use abbreviations when using the frameworks component classes for example.
# If wrapper objects like the 'Conv' instance shown below are used, the Registry
# class of the framework takes care that label identification strings are unique.
# When reading in data like for the actual_value shown below, it is good to convert the
# pandas series object to an plain array in order to avoid errors.
# When using PyCharm, while pointing on a component and pressing 'ctrl+Q' usually pops
# up the 'Quick definition' window, that presents information about the class and possible parameters
# at point.
source_1 = Conv("Pv_1",
                out={bel: Flow(actual_value=data["pv"].values,
                               nominal_value=6000000,
                               fixed=True)})

# In the shown source object above, one can already see how Oemof's Flow objects work regarding
# the scaling of a stream:
# - 'nominal_value' works as a factor multiplied to 'actual_value'
# - 'fixed' indicates a Flow that can not be dispatched or reduced
# - 'fixed_costs' are operating expenses that have to be paid regardless of whether the source
# in this case provides a positive power gradient or not.
# - 'actual_value' is only specified if the component is know prior to solving

# We can gather created components using the 'registry' dictionary of the 'Registry' class:
bel_grid = Registry.registry.get("Grid")
# Or we could access the '.components' container of the hub object using a subscription:
bel_grid = hub_1["Grid"]

Conv("Grid_source",
     out={bel_grid: Flow(nominal_value=1000 * data["pv"].max(),
                         variable_costs=1)})

# We can create a transformer object providing input and output stream definitions.
# For custom components, we can use abbreviated input and output definitions, in case
# we do not want to specify the then automatically added Flow object with additional parameters.
# For a transformer object, the specification of conversion factors is mandatory in order
# to work within Oemof properly.
trafo_1 = Conv("Trafo",
               inp=bel_grid,
               out=bel,
               conversion_factors={bel: 0.4})

# We can provide additional Flow objects parameters by either initializing a Flow object directly,
# or by providing the Flow objects parameters as part of the kwargs dictionary. For normal usage in a model
# this is not particularly interesting, however, when reading components parameters from a file, this is important.
# I.e. you could have a .csv file in which one column is labelled 'out_nominal_value', this will then be
# added as input for the created flow object at object initialization time.
immersion_heater = Conv("Immersion_heater",
                        inp=bel,
                        out=bheat,
                        conversion_factors={bheat: 0.85},
                        out_nominal_value=3 * data["demand_el"].max(),
                        out_variable_cost=0.2)

# We can control that the input flow objects nominal value has been correctly set using the .out_flow
# attribute. This allows for direct access to the output stream's flow object:
print(immersion_heater.out_flow.__dict__)

demand_heat = Sink("Demand heat",
                   inp={bheat: Flow(nominal_value=2,
                                    actual_value=data["demand_el"].values,
                                    fixed=True)})

demand_el = Sink("Demand el",
                 inp={bel: Flow(nominal_value=1,
                                actual_value=data["demand_el"].values,
                                fixed=True)})

hub_1.optimize()

# This methodology can be used for initializing components from .csv files as it is done in the
# ConvFromDB class. This only works of the specified bus types can be found in the hub's component
# list, e.g. in the case below, the 'air source heat pump 22' has got 'inp=Tamb', 'out'='heat'. Hence,
# a bus with type 'heat' has to be present in order to be able to connect it properly.
heat_pump = ConvFromDB("air source heat pump 22", hub_1)

# The initialization parameters can be accessed using the .parameters attribute:
print(heat_pump.parameters)

# Or we can directly provide an additional kwargs argument in order to specify the bus to which
# this transformer instance should be connected to:
heat_pump_2 = ConvFromDB("air source heat pump 22", hub_1, out_bus="Heat")

# We can access the input and output streams of a regular component using the .inp and .out
# attributes:
print(immersion_heater.inp)
print(immersion_heater.out)

# Or we can directly access the flow objects of the streams:
print(immersion_heater.inp_flow)
print(immersion_heater.out_flow)

# FIXME: This is buggy at the moment
# If we want to reconfigure a hub in order to build another energy system scenario, e.g. without
# one specific component in the system, we can reinitialise a hub providing the list of components
# we want to use:
components = hub_1.components
del components["air source heat pump 22"]
hub_2 = Hub("Variation of hub_1", components=components)
print(hub_2.components)

# Or we can just copy a hub. This calls the __deepcopy__ method of the hub, returning a clone
# of itself and clones of all of its components. This at the moment only works for hub's that
# have not yet been optimized:
hub_3 = hub_1.copy()
hub_3.optimize()
hub_3.plot()

# One can read all the components initialisation parameters using the Registry.parameter_registry dictionary:
print(Registry.parameter_registry)
# Or we can use the convenience of using pandas to return this dictionary in form of a pandas DataFrame object:
print(Registry.return_dataframe())
# We can also save this DataFrame directly using:
Registry.save_dataframe()
