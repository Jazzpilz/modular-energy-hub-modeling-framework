Modular Energy Hub Modelling Framework
======================================

This project is part of Google Summer of Code. All what has been done within 2016' Google Summer of Code is before the last commit 'End of Google Summer of Code'.

Objectives of this framework
----------------------------

0. Components shall be implemented in object-oriented fashion
0. A modelling framework that can easily construct and configure systems of modular entities
0. Allow systems to be duplicated, reconfigured and reused in larger models (Hierarchically nest-able)
0. Configure parameters readable from an external table or a database
0. Approximation of sub-systems (model solvability)

Documentation
-------------

Find the [documentation](/documentation/documentation.pdf) in the directory ./documentation/Documentation.pdf

Installation
------------

Dependencies of this project:

- [Oemof 0.1](https://github.com/oemof/oemof), see [Oemof directory within the framework](./oemof)

- [Pyomo](http://www.pyomo.org/)

- [Pandas](http://pandas.pydata.org/)

Not necessarily required:

- [GraphViz](http://graphviz.org/)

Clone the repository and install using a path to the repository:
```
git clone https://Jazzpilz@bitbucket.org/Jazzpilz/modular-energy-hub-modeling-framework.git

sudo pip3 install -e /path/to/the/repository
```

Under MS Windows, skip the 'sudo'

Notes:

- Oemof version 0.1 is mandatory for this framework to work, it is contained within the frameworks directory

- If you want to use another Oemof version, copy the content of [CustomModel](/interface/custommodel.py) to /path/to/oemof/oemof/solph/models.py

- Oemof and hence also this framwork uses Python 3.xx

A recommended configuration:

- Anaconda 3.4
- Linux Mint 17.x
- Pycharm Community 2016.1.x
- GraphViz 2.38


Examples
--------

0. Example_Simple: Show basic functionality of the framework
0. Example_Structure_Plotting: A simple example of a structural plot
0. Example_Components: Illustrate different ways to create components of an energy system
0. Example_Nesting: Illustrate the nesting capabilities of the framework
0. Example_Constraints: Illustrate how to add additional constraints on top of an existing model

Main functions and classes to be used
-------------------------------------
- Hub: Modelling of energy systems
- CopyHub: Nesting of energy systems and subsequent CopyHub instances alike
- Bus, Conv, Sink, Store: Used in order to model energy system components
- ConvFromDB, SinkFromDB, StoreFromDB: Specify energy system components reading specification data from file
- Size: Scale energy system components providing arbitrary parameters to this object. Enables for bundling component specifications in one position in code
- CustomModel: Interface to a custom Oemof model class 'CustomModel', a variation of Oemof's 'OperationalModel' class. Allows for specifying constraints on top of an existing model.
- OperationalModel : Interface to Oemof's 'OperationalModel' class.
- show_tree :: Plot a tree structure of the energy system's structure

See also [Project code structure](./documentation/project_structure.pdf).

Status: Achieved
----------------

- Dependencies: 'Oemof' is used as basis on which this framework builds upon, whichever builds upon 'Pyomo'

- Component registry: 'Registry'
This class stores manifold information about components within an energy system. Using this information, it automatically assigns unique labels to components as this is a prerequisite for 'Oemof'. When energy systems are scaled using the classes described below, this comes in handy. Registry acts like a central storage for information shared among the model.

- Energy system and container for the later: 'Hub' and 'CopyHub'
'Hub' is a class for the modelling of energy systems, 'CopyHub' is a container for subsequent instances of 'CopyHub' or 'Hub' alike. 'Hub' as well as 'CopyHub' provide methods that organise the copying process, thus allowing for cloning of themselves and all stored components or energy systems. This facilitates the process of creating large (nested) energy systems. 'CopyHub' provides a method '.networklinks' that allows for specifying connections between busses in nested system structures.
Following the main principle stated above, both classes provide only a number of methods that should provide a good starting point to create energy systems of larger structures. Additional methods or 'convenience' methods can be build upon this structure along with growing functionality of 'Oemof'.

- Energy systems components: 'Bus' (stream transport), 'Conv' (converters), 'Sink' (energy sink) and 'Store' (energy storage)
Except for 'Bus' which inherits from 'solph.Bus' of 'Oemof', these are interface classes to 'Oemof' component classes. They add functionality on top of these classes, enabling for easier access to flow specifications or flow objects. These components can be specified also without initialising an instance of a 'solph.Flow' object at instantiation time, allow for specifying a component including related 'Flow' objects using i.e. a database.

- Specifying components from database files: 'ConvFromDB', 'SinkFromDB', 'StoreFromDB'
As described in paragraph above, components can be initialised using specifications read from disk. These classes search for busses in a 'Hub' that match a specified type or one can directly specify to which bus the component should connect to.

For a detailed breakdown of what has been done so far, see [STATUS.md](STATUS.md) in the project folder.

Status: Pending
---------------
- Removal of components of a Hub instance: At the moment Hub does not yet support the removal of components as soon as they are attached to it. This is due to that Hub inherits from Oemof's 'solph.EnergySystem' class, inheriting this behaviour as well

- Facilitation of adding of constraints: At the moment, one need to understand Oemof and Pyomo in order to be able to add additional constraints on top of a model. Most likely this will remain a requirement, but as intended, it would be nice to be able to naturally specify a restriction on two already specified variables providing a mathematical relation expression, as one can do using Pyomo (i.e. Add_Constraint(pvsize + st.size <= 10))

- Variable domain specifications: At the moment, it is not possible to specify the domain of certain variables within Oemof. Variables are declared in different locations in the Oemof package and often include one set for a bunch of variables. For example, a variable is declared for all flows. If one would like to declare a certain flow to be within positive integers, this is not possible with the current implementation.

- Attribute management of components: When attributes of components are altered, one needs to maintain track of these changes. In order to do so, the parameters of the component have to be known. There is the need for a method that returns a current dictionary or the like of parameters which can then be used to copy components. At the moment, components are copied using their initialisation parameters. If the parameters of the component have been altered, the assigned dictionary .parameters does not reflect these changes.


Contributors
------------
- M.Sc. Frederik Banis
- M.Eng. Julien Marquant
- Prof. Dr. Ralph Evins
- Dr. Andrew Bollinger
- Dr. Stefan Pfenninger
- M.Eng. Simon Hilpert
