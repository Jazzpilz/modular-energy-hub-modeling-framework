"""
A simple example of a structural plot.

@author: Frederik Banis
"""

from interface.components import Bus, Conv, Sink, Store
from interface.hub import Hub, CopyHub
from interface.miscellaneous import show_tree

hub = Hub("House")
bel = Bus("Electric", type="el")
bel_2 = Bus("Electric", type="el")
conv = Conv("Convertor", inp="sun", out=bel_2)

district = CopyHub(hub, 2, label="District")
city = CopyHub(district, 2, label="City")

show_tree(label="This is a simple plotting example")