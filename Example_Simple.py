"""
Show basic functionality of the framework

@author: Frederik Banis
"""

# Framework objects and functions
from interface import Flow, outputlib
from interface.model import CustomModel
from interface.hub import Hub
from interface.components import Bus, Conv, Sink
from interface.miscellaneous import show_tree

# Additional imports
import os
import pandas as pd
import matplotlib.pyplot as plt

# ##########################################################################
# Read input data
# ##########################################################################

csv_file = os.path.join(os.getcwd(), 'data', 'storage_invest.csv')
data = pd.read_csv(csv_file, sep=',').iloc[:, 1:]
print(data)

# for col in ['pv', 'demand_el']:
#     plot = data[col].plot(title=col)
#     plot.set_xlabel("Hours")
#     plt.show()

# ##########################################################################
# Initialize model and components
# ##########################################################################

# Only horizon is a required parameter for the CustomModel class, it needs to be a pandas date_range or similar
model = CustomModel(horizon=pd.date_range('1/1/2012', periods=len(data), freq='H'),
                    solver='glpk',
                    solve_kwargs={'tee': True})

# Create a Hub, components below are automatically added to it
house = Hub("House")

# Create a Bus object
bel = Bus("Electricity",
          type="Electric")

# Create a solph.Source object using the Conv class, label is "PV", inp parameter could be dropped as well
# Flow; A solph.Flow object that specifies a flow between two nodes (components respectively) in an energy system
# actual_value: Actual value for the given timestep
# nominal_value: Factor multiplied with actual_value in order to yield the final value of the component (sizing parameter)
# fixed: Marks this component as fixed within the optimization problem
pv = Conv("PV",
          inp="Rad",
          out={bel: Flow(actual_value=data["pv"],
                         nominal_value=582000,
                         fixed=True)})

# Create a solph.Source object using the Conv class, Source can deliver the maximal demand
# This component is sized within the optimization problem in order to meet the overall demand
# nominal_value: Sizing parameter, specifies the maximal power this component can provide
grid = Conv("Grid",
            inp="Grid",
            out={bel: Flow(nominal_value=data["demand_el"].max())})

# Create a fixed solph.Sink object using the Sink class
# nominal_value: As this parameter is set to 1, the final value of this component equals the provided data
demand = Sink("Demand",
              inp={bel: Flow(actual_value=data["demand_el"],
                             nominal_value=1,
                             fixed=True)})

# ##########################################################################
# Optimize the problem and plot results as well as hub/component structure
# ##########################################################################

# Call the .optimize method of the house hub
house.optimize()

# Plot result data and save the plot to disk (current working directory of this script)
house.plot(ylabel='W', plot_format='pdf')

# Plot a tree with project components, requires the GraphViz package installed and accessible by 'PATH'
# show_tree()

