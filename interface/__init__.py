from oemof import outputlib
from oemof.tools import logger, helpers
import logging as logging
import oemof.solph as solph
from oemof.solph.network import Flow as Flow

import os
import pandas as pd

database_path = os.path.join(os.getcwd(), 'data', 'database')


class Registry(object):
    registry = {}
    label_registry = {}  # Keep track of all labels
    component_count = {}  # Count every component
    hub_structure_registry = {'levels': {}, 'hubs': []}  # Register all structural information regarding hubs
    bus_connections = {}  # Register all bus connections carried out via .networklinks
    parameter_registry = {}  # Register initialisation parameters of all components

    model = None

    @staticmethod
    def register(obj=None, cls=None, label=None, **kwargs):
        """
        Returns unique identification strings.

        Dictionary label_registry and component_count in Registry object namespace is used
        in order to register identification strings as well as components.

        Dictionary hub_structure_registry is used to register structural information regarding hubs.

        Dictionary bus_connections is used to register structural information regarding bus connections.

        Parameters
        ----------
        obj : object
            Used to register an EnergySystem as an Hub object or an CopyHub object in the class dictionary
             hub_structure_registry.

        cls : class
            A classname used to register a component in case no label has been specified.

        label : string or None or dict
            Identification string or None type. If identification string exists already it
            is unified, if None type is passed an unique idendef manage_inputs(**kwargs):

        """
        classname = Registry._get_classname(cls)

        label_idx = Registry.remove_indexer(label)
        label, indexer = label_idx

        if type(label) is dict:  # Resolve deepcopy issue
            label = label.get('label')

        if not label:
            label = Registry._set_label_by_classname(classname)
        elif label in Registry.label_registry:
            label = Registry._uniquify_label(label)
        else:
            label = Registry._initialize_label(label, indexer)

        if not kwargs.get('suppress_registry'):
            Registry._register_component(obj, classname, label=label)

        return label

    @staticmethod
    def _get_classname(cls):
        if cls:
            classname = str(cls)[:-2]
            classname = classname[classname.rfind('.') + 1:]
        else:
            classname = 'Component'
        return classname

    @staticmethod
    def remove_indexer(label):
        indexer = None
        try:
            index = label.rfind('_')
            if index is not -1:
                try:
                    indexer = label[index + 1:]
                    int(indexer)
                    label = label[:index]
                except ValueError:
                    indexer = None
        except AttributeError:
            pass
        return (label, indexer)

    @staticmethod
    def _set_label_by_classname(classname):
        if not Registry.label_registry.get(classname):
            Registry.label_registry[classname] = 0
        else:
            Registry.label_registry[classname] += 1
        return classname + '_' + str(Registry.label_registry[classname])

    @staticmethod
    def _uniquify_label(label):
        Registry.label_registry[label] += 1
        label = label + '_' + str(Registry.label_registry[label])
        return label

    @staticmethod
    def _initialize_label(label, indexer=None):
        Registry.label_registry.update({label: 0})

        if indexer:
            label = label + '_' + indexer

        return label

    @staticmethod
    def _register_component(obj, classname, label):
        try:
            Registry.component_count[classname] += 1
        except KeyError:
            Registry.component_count.update({classname: 1})

        Registry.registry.update({label: obj})

        if 'EnergySystem' in str(classname):
            hub_registry = Registry.hub_structure_registry.get('hubs')
            hub_registry.append(obj)

        if 'CopyHub' in str(classname):
            Registry.hub_structure_registry.get('hubs').append(obj)

    @staticmethod
    def return_dataframe():
        return pd.DataFrame.from_dict(Registry.parameter_registry)

    @staticmethod
    def save_dataframe(file_path='data', file_name='components_configuration.csv'):
        df = pd.DataFrame.from_dict(Registry.parameter_registry)
        df.to_csv(os.path.join(os.getcwd(), file_path, file_name))
