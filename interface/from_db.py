import os
import pandas as pd

from interface.components import Conv, Sink, Store
from interface import database_path


# TODO implement
class SelectionFromDB(object):
    def __init__(self):
        pass


class ConvFromDB(object):
    def __new__(cls, key, hub, **kwargs):
        """
        This class enables for specifying a converter (a source or transformer object)
        via reading from a file. In case no keyword argument 'path' to the specification
        file's folder is given, a standard database path is taken from the __init__ file
        of the package ('/data/Conv/', relative to the current working directory.

        The algorithm searches for bus types in the .components list of the Hub instance
        to connect to.
        :param key: (str)
            A string label specifying the component to gather data from
        :param hub: (instance of Hub)
            An instance of Hub
        :param kwargs: (dict)
            Keyword arguments dictionary, covered values are:
            'path': Path to the data,
            'inp_bus': Input bus object
            'out_bus': Output bus object
        :return: An instance of Conv
        """
        # Select a proper path to the specification file
        if kwargs.get('path'):
            path = kwargs.get('path')
        else:
            path = ConvFromDB._get_database()

        # Read the file
        file = os.path.join(path, 'Conv.csv')
        df = pd.read_csv(file)
        # Remove whitespace in column labels
        df = df.rename(columns=lambda x: x.strip().rstrip())
        # Remove whitespace from all other cells:
        for column in df.columns:
            try:
                df.loc[:, column] = df.loc[:, column].apply(lambda x: x.strip().rstrip())
            except AttributeError:
                pass

        # Set index to label for convenient access to the data
        df = df.set_index("label")

        # create a dictionary from columns and their values in the 'key' row
        for column in df.columns:
            # Do not consider "technology general name" column
            if "technology general name" not in column:
                value = df.loc[key, column]
                kwargs.update({column: value})

        # Create a separate dictionary for both streams containing the bus types to connect to
        param_dict = {}
        for item in ['inp', 'out']:
            value = kwargs.get(item)
            param_dict.update({item: value})
            if value:
                kwargs.pop(item)

        inp_bus_list = []
        out_bus_list = []
        inp_bus = kwargs.get('inp_bus')
        out_bus = kwargs.get('out_bus')

        # Gather busses by label
        if out_bus:
            out_bus_list.append(hub.components[out_bus])
            if inp_bus:
                inp_bus_list.append(hub.components[inp_bus])

        # Search for busses in hub.components to connect to
        else:
            for component in hub.components:
                try:
                    if component.type == param_dict.get('inp'):
                        inp_bus_list.append(component)
                except AttributeError:
                    pass

                try:
                    if component.type == param_dict.get('out'):
                        out_bus_list.append(component)
                except AttributeError:
                    pass

        if len(inp_bus_list) == 1 and len(out_bus_list) == 1:
            # Cover 'transformer' component creation
            inp = inp_bus_list[0]
            out = out_bus_list[0]

            conv = Conv(label=key,
                        inp=inp,
                        out=out,
                        **kwargs)

        elif len(out_bus_list) == 1:
            # Cover 'source' component creation
            out = out_bus_list[0]
            conv = Conv(label=key,
                        inp=param_dict.pop('inp'),
                        out=out,
                        **kwargs)

        else:
            raise ValueError("ConvFromDB: There are multiple or no Bus objects to connect to "
                             "(matching or not matching provided type string). Input and "
                             "output bus types: {0}, {1}".format(param_dict.get("inp"), param_dict.get("out")))

        return conv

    @staticmethod
    def _get_database():
        """
        Provide the path to the database.
        :return: Path to the database
        """
        return os.path.join(database_path, 'Conv')


class SinkFromDB(object):
    def __new__(cls, key, hub, **kwargs):
        """
        This class enables for specifying a sink via reading from a file. In case no
        keyword argument 'path' to the specification file's folder is given, a standard
        database path is taken from the __init__ file of the package ('/data/Sink/',
        relative to the current working directory.

        The algorithm searches for bus types in the .components list of the Hub instance
        to connect to.
        :param key: (str)
            A string label specifying the component to gather data from
        :param hub: (instance of Hub)
            An instance of Hub
        :param kwargs: (dict)
            Keyword arguments dictionary, covered values are:
            'path': Path to the data,
            'inp_bus': Input bus object
        :return: An instance of Conv
        """
        # Select a proper path to the specification file
        if kwargs.get('path'):
            path = kwargs.get('path')
        else:
            path = SinkFromDB._get_database()

        # Read the file
        file = os.path.join(path, 'Sink.csv')
        df = pd.read_csv(file)
        # Remove whitespace in column labels
        df = df.rename(columns=lambda x: x.strip().rstrip())
        # Remove whitespace from all other cells:
        for column in df.columns:
            try:
                df.loc[:, column] = df.loc[:, column].apply(lambda x: x.strip().rstrip())
            except AttributeError:
                pass

        # Set index to label for convenient access to the data
        df = df.set_index("label")

        # create a dictionary from columns and their values in the 'key' row
        for column in df.columns:
            # Do not consider "technology general name" column
            if "technology general name" not in column:
                value = df.loc[key, column]
                kwargs.update({column: value})

        # Create a separate dictionary for both streams containing the bus types to connect to
        param_dict = {}
        for item in ['inp']:
            value = kwargs.get(item)
            param_dict.update({item: value})
            if value:
                kwargs.pop(item)

        inp_bus_list = []
        inp_bus = kwargs.get('inp_bus')

        # Gather busses by label
        if inp_bus:
            inp_bus_list.append(hub.components[inp_bus])

        # Search for busses in hub.components to connect to
        else:
            for component in hub.components:
                try:
                    if component.type == param_dict.get('inp'):
                        inp_bus_list.append(component)
                except AttributeError:
                    pass

        if len(inp_bus_list) == 1:
            # Cover 'transformer' component creation
            inp = inp_bus_list[0]

            sink = Sink(label=key,
                        inp=inp,
                        **kwargs)

        else:
            raise ValueError("SinkFromDB: There are multiple or no Bus objects to connect to "
                             "(matching or not matching provided type string). Input "
                             "bus types: {0}, {1}".format(param_dict.get("inp")))

        return sink

    @staticmethod
    def _get_database():
        """
        Provide the path to the database.
        :return: Path to the database
        """
        return os.path.join(database_path, 'Sink')


class StoreFromDB(object):
    def __new__(cls, key, hub, **kwargs):
        """
        This class enables for specifying a storage via reading from a file.
        In case no keyword argument 'path' to the specification file's folder is given,
        a standard database path is taken from the __init__ file of the package
        ('/data/Store/', relative to the current working directory.

        The algorithm searches for bus types in the .components list of the Hub instance
        to connect to.
        :param key: (str)
            A string label specifying the component to gather data from
        :param hub: (instance of Hub)
            An instance of Hub
        :param kwargs: (dict)
            Keyword arguments dictionary, covered values are:
            'path': Path to the data,
            'inp_bus': Input bus object
            'out_bus': Output bus object
        :return: An instance of Conv
        """
        # Select a proper path to the specification file
        if kwargs.get('path'):
            path = kwargs.get('path')
        else:
            path = StoreFromDB._get_database()

        # Read the file
        file = os.path.join(path, 'Store.csv')
        df = pd.read_csv(file)
        # Remove whitespace in column labels
        df = df.rename(columns=lambda x: x.strip().rstrip())
        # Remove whitespace from all other cells:
        for column in df.columns:
            try:
                df.loc[:, column] = df.loc[:, column].apply(lambda x: x.strip().rstrip())
            except AttributeError:
                pass

        # Set index to label for convenient access to the data
        df = df.set_index("label")

        # create a dictionary from columns and their values in the 'key' row
        for column in df.columns:
            # Do not consider "technology general name" column
            if "technology general name" not in column:
                value = df.loc[key, column]
                kwargs.update({column: value})

        # Create a separate dictionary for both streams containing the bus types to connect to
        param_dict = {}
        for item in ['inp', 'out']:
            value = kwargs.get(item)
            param_dict.update({item: value})
            if value:
                kwargs.pop(item)

        inp_bus_list = []
        out_bus_list = []
        inp_bus = kwargs.get('inp_bus')
        out_bus = kwargs.get('out_bus')

        # Gather busses by label
        if out_bus:
            out_bus_list.append(hub.components[out_bus])
            if inp_bus:
                inp_bus_list.append(hub.components[inp_bus])

        # Search for busses in hub.components to connect to
        else:
            for component in hub.components:
                try:
                    if component.type == param_dict.get('inp'):
                        inp_bus_list.append(component)
                except AttributeError:
                    pass

                try:
                    if component.type == param_dict.get('out'):
                        out_bus_list.append(component)
                except AttributeError:
                    pass

        if len(inp_bus_list) == 1 and len(out_bus_list) == 1:
            # Cover 'transformer' component creation
            inp = inp_bus_list[0]
            out = out_bus_list[0]

            store = Store(label=key,
                          inp=inp,
                          out=out,
                          **kwargs)

        elif len(out_bus_list) == 1:
            # Cover 'source' component creation
            out = out_bus_list[0]
            store = Store(label=key,
                          inp=param_dict.pop('inp'),
                          out=out,
                          **kwargs)

        else:
            raise ValueError("StoreFromDB: There are multiple or no Bus objects to connect to "
                             "(matching or not matching provided type string). Input and "
                             "output bus types: {0}, {1}".format(param_dict.get("inp"), param_dict.get("out")))

        return store

    @staticmethod
    def _get_database():
        """
        Provide the path to the database.
        :return: Path to the database
        """
        return os.path.join(database_path, 'Store')
