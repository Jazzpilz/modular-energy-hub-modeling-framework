from interface import Registry

import oemof.solph as solph
from oemof.solph import blocks
from oemof.solph.network import Storage
from oemof.solph.options import Investment

import copy
import os


class Size(object):
    def __init__(self, label=None, domain='continuous', **kwargs):
        """
        A sizing variable class. Opts for providing a flexible possibility for storing, scaling and assignment of
        parameters to energy system components.
        :param label: (str) An identification string
        :param domain: (str) A string indicating the domain of the sizing variable, either 'integer' or 'continuous'.
        :param kwargs: (dict) A dictionary with parameters of the sizing variable. All parameters in this dictionary
        are assigned as attributes of the sizing variable, {"cost": 200} translates to the attribute .cost with the
        value 200.
        """
        self.label = Registry.register(self, Size, label=label)
        self.domain = domain
        self._parameters = kwargs

        for argument, value in kwargs.items():
            setattr(self, argument, value)

        # Additional attributes
        self._scalable = []

    @property
    def attributes(self):
        attr_list = [attr for attr in [attr for attr in dir(self) if not attr is 'attributes'] if
                     not attr is 'scalable']
        attr_list = [attr for attr in attr_list if
                     not attr.startswith('__') and not attr.startswith('_') and not callable(getattr(self, attr))]

        value_list = [getattr(self, attr) for attr in attr_list]

        attributes = dict(zip(attr_list, value_list))

        return attributes

    @property
    def scalable(self):
        for attribute in self.attributes.keys():
            value = self.attributes.get(attribute)
            try:
                try:
                    int(value)
                    self._scalable.append(attribute)
                except ValueError:
                    pass
            except TypeError:
                pass
        return self._scalable

    def reinitialize(self):
        return copy.deepcopy(self)

    def __mul__(self, factor):
        try:
            factor = factor[0]
            new = self
        except TypeError:
            new = self.reinitialize()

        for attribute in new.scalable:
            value = getattr(new, attribute)
            setattr(new, attribute, value * factor)
        return new

    def __rmul__(self, factor):
        try:
            factor = factor[0]
            new = self
        except TypeError:
            new = self.reinitialize()

        for attribute in new.scalable:
            value = getattr(new, attribute)
            setattr(new, attribute, value * factor)
        return new


# TODO implement Units
class Units(object):
    def __init__(self):
        pass


def show_tree(label=None, filename="tree_plot", show_connections=False, **kwargs):
    """
    A function plotting a tree graph illustrating the projects nesting structure of CopyHub or Hub objects and their
    components.
    :param label: (str) A label shown at the final plot
    :param filename: (str) A filename for the plot
    :param show_connections: (bool) Show connections between Bus objects
    :param kwargs: (dict) Providing the key 'format' with a format string, e.g. '.svg' or '.pdf' allows for specifying
     the format the final plot is stored in.
    """
    import graphviz as gv
    import functools

    format = kwargs.get('format')
    view = kwargs.get('view', True)

    if format:
        graph = functools.partial(gv.Digraph, format=format)
    else:
        graph = functools.partial(gv.Digraph, format='pdf')

    overall_styles = {"graph": {"label": label,
                                "fontsize": "16"}}
    copy_hub_style = {"size": "16",
                      'fontname': 'Helvetica',
                      'fontcolor': 'black',
                      'color': 'black',
                      'shape': 'hexagon',
                      'style': 'filled',
                      'fillcolor': '#f3fff3'}
    hub_style = {"size": "16",
                 'fontname': 'Helvetica',
                 'fontcolor': 'black',
                 'color': 'black',
                 'style': 'filled',
                 'fillcolor': '#f3fff3'}
    component_style = {"size": "10",
                       "color": "grey",
                       "pin": "True",
                       'style': 'filled',
                       'fillcolor': '#f3fff3'}
    edge_style = {'style': 'dashed',
                  'color': 'black',
                  'arrowsize': "0",
                  'fontname': 'Courier',
                  'fontsize': '12',
                  'fontcolor': 'white',
                  "dir": None}
    edge_connected_style = {'color': 'grey',
                            'arrowhead': 'open',
                            "arrowsize": "1",
                            'fontname': 'Courier',
                            'fontsize': '12',
                            'fontcolor': 'white',
                            "dir": "both"}

    nodes = []  # list of nodes
    edges = []  # list of tuples
    for hub in Registry.hub_structure_registry.get("hubs"):
        if "CopyHub" in str(hub):
            copy_hub_obj = hub
            for hub in copy_hub_obj:
                specifier = (copy_hub_obj.label, copy_hub_style)
                nodes.append(specifier)
                edges.append(((copy_hub_obj.label, hub.label), edge_style))
        else:
            specifier = (hub.label, hub_style)
            nodes.append(specifier)
            for component in hub.components:
                try:
                    label = component.label
                except AttributeError:
                    label = component

                try:
                    # Exclude a LinearTransformer objects when they are part of a networklink
                    if component.networklinks:
                        pass
                except AttributeError:
                    specifier = (label, component_style)
                    nodes.append(specifier)
                    edges.append(((hub.label, label), edge_style))

    def add_nodes(graph, nodes):
        for n in nodes:
            if isinstance(n, tuple):
                graph.node(n[0], **n[1])
            else:
                graph.node(n)
        return graph

    def add_edges(graph, edges):
        for e in edges:
            if isinstance(e[0], tuple):
                graph.edge(*e[0], **e[1])
            else:
                graph.edge(*e)
        return graph

    def apply_styles(graph, styles):
        graph.graph_attr.update(('graph' in styles and styles['graph']) or {})
        return graph

    graph = add_edges(add_nodes(graph(), nodes), edges)
    graph = apply_styles(graph, styles=overall_styles)

    if show_connections:
        directed_edges = list(Registry.bus_connections.keys())
        directed_edges = [((bus_1.label, bus_2.label), edge_connected_style) for bus_1, bus_2 in directed_edges]
        graph = add_edges(graph, directed_edges)

    graph.render(os.path.join("plot", str(filename)), view=view)
