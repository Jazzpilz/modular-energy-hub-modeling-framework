from interface import Registry, outputlib, solph
from interface.components import Bus, Conv, Sink, Store
from interface.model import CustomModel

import oemof.solph as solph

import os
import copy
import sortedcontainers
import matplotlib.pyplot as plt
import seaborn as sns

# For Components.pop(); Can be removed in case .pop() should not be specified outside sortedcontainers.SortedDict
NONE = object()


class Hub(solph.EnergySystem):
    def __init__(self, label=None, components=[], **kwargs):
        """
        A node in an energy system's network, e.g. a house.
        Inherits from the solph EnergySystem class.
        :param label: (str) An identification string
        :param components: (list) A list of components that should be assigned to this Hub object
        :param kwargs: (dict) Keyword arguments dictionary, solph.EnergySystem (super) is initialized with it
        """
        # _parameters allow for reusing the initialization parameters
        self._parameters = copy.deepcopy(kwargs)
        self._parameters.update(label=label,
                                components=components)

        self.label = Registry.register(obj=self,
                                       cls=solph.EnergySystem,
                                       label=label, **kwargs)

        try:
            self.time_idx = CustomModel.horizon
        except AttributeError:
            self.time_idx = [0]

        self.level = 0

        # Initializing solph.EnergySystem
        kwargs.update({'label': self.label})
        kwargs.update({'time_idx': self.time_idx})
        kwargs.update({'groupings': solph.GROUPINGS})
        super().__init__(**kwargs)

        self._components = None

        if isinstance(components, Components):
            for component in components.values():
                self.add(component)
        # Cover the case when components are assigned as list
        elif type(components) is list:
            for component in components:
                self.add(component)
        # Cover the case when only one component is assigned
        else:
            self.add(components)

        self.results_df = None

    def __getitem__(self, item):
        return self.components[item]

    def add(self, entity):
        """
        Calls .add method of EnergySystem base class.

        Parameters
        ----------
        entity : (obj)
            Oemof energy system component, e.g. a Bus
        """
        super().add(entity)

    def assign_results(self):
        """
        Assigns pandas DataFrame's holding the result data for each component in the
        Hub to the corresponding component.
        Results can be then be accessed like: Hub.components[0].input or
        Hub.components[0].from_bus.
        """
        results = self.results_df = outputlib.ResultsDataFrame(energy_system=self)

        stream_mapping = {"from_bus": "input", "to_bus": "output"}
        for stream in ["from_bus", "to_bus"]:
            for component in self.components:
                try:
                    result = results.slice_by(obj_label=component.label, type=stream)
                    setattr(component, "%s" % (stream), result)
                    setattr(component, "%s" % (stream_mapping.get(stream)), result)
                except KeyError:
                    pass

    @property
    def components(self):
        """
        Maps self._components to self.entities (attribute assigned by parent class) using
        the Components class. Components allows for index/string access of components in the
        _components list
        :return: self._components, Components sortedDict container, holding the components assigned
        to the Hub object
        """
        entities = self.entities

        if len(entities) is 0:
            self._components = None
        else:
            self._components = Components(self, components=entities)
        return self._components

    @components.setter
    def components(self, entities):
        """
        Defines how components are treated when assigned to the attribute .components of Hub
        :param entities: (list) of entities that shall be added to the Hub object
        """
        key_list = []
        for key, value in self.groups.items():
            key_list.append(key)

        for key in key_list:
            del self.groups[key]

        self.entities = []

        for entity in entities:
            self.add(entity)

    # FIXME Get to work in the future
    def align_object_containers(self, components):
        """
        Aligns 'entities' and 'groups' object containers of the parent class to the 'components'
        container.

        Note
        ----
        Oemof uses the list 'entities' and the dictionary 'groups' to keep track of components
        attached to the energy system (hub). In this class 'Hub', we want to make use of the
        'Components' class instead. We have to make sure that before the solver is called
        on an instance of Hub, all containers represent the same objects in order to work.
        """

        self.entities = list(components)

        key_list = []
        for key, value in self.groups.items():
            key_list.append(key)

        for key in key_list:
            del self.groups[key]

        for component in list(components):
            self.add(component)

    def optimize(self, print_output=True):
        """
        Run the solver on the Hub object itself
        :param print_output: (bool) Specifies whether the solver information shall
         be printed out
        :return: Solver information
        """

        # self.align_object_containers(components=self.components)

        if Registry.model.optimize_counter > 1:
            Registry.model.objective_function(update=True)

        output = Registry.model.run_solve(energysystem=self)
        self.assign_results()

        if print_output is True:
            print(output)

        return output

    def plot(self, xlabel='Date', ylabel=None, tick_distance=None, save_plot=True, force_show=False, plot_format='jpg'):
        """
        Plots input and output streams for each bus in the Hub. Builds upon outputlib.DataFramePlot class of Oemof.
        Uses Matplotlib and Seaborn for plotting.
        :param xlabel: (str) A label for the x axis
        :param ylabel: (str) A label for the y axis
        :param tick_distance: (int) Tick distance in hours.
        Passed to the outputlib.DataFramePlot.set_datetime_ticks method of Oemof.
        :param save_plot: (bool) Whether the plots shall be saved to the calling scripts directory
        """
        sns.set_style("whitegrid")
        sns.set_context("paper", rc={"grid.linewidth": .2})
        sns.set_palette("hls")

        plot = outputlib.DataFramePlot(energy_system=self)
        busses = plot.index.levels[0].values
        for bus_label in busses:

            try:
                plot.slice_unstacked(bus_label=bus_label, type="to_bus")
                try:
                    plot.plot(title="Input to bus: {0}".format(bus_label), linewidth=2)
                    if ylabel:
                        plot.ax.set_ylabel(ylabel)
                    plot.ax.set_xlabel(xlabel)
                    plot.set_datetime_ticks(date_format='%d-%m-%Y', tick_distance=tick_distance)

                    sns.despine()
                    plt.tight_layout()

                    if save_plot:
                        plt.savefig(
                            os.path.join(os.getcwd(), 'plot', "bus_{0}_input.{1}".format(bus_label, plot_format)))

                    if force_show is True:
                        plt.show()
                    elif len(busses) >= 5:
                        pass
                    else:
                        plt.show()

                    plt.close()
                except TypeError as e:
                    print("Hub.plot(): %s" % e)
            except KeyError:
                print("Hub.plot(): No input stream")

            try:
                plot.slice_unstacked(bus_label=bus_label, type="from_bus")
                try:
                    plot.plot(title="Output from bus: {0}".format(bus_label), linewidth=2)
                    if ylabel:
                        plot.ax.set_ylabel(ylabel)
                    plot.ax.set_xlabel(xlabel)
                    plot.set_datetime_ticks(date_format='%d-%m-%Y', tick_distance=tick_distance)

                    sns.despine()
                    plt.tight_layout()

                    if save_plot:
                        plt.savefig(
                            os.path.join(os.getcwd(), 'plot', "bus_{0}_output.{1}".format(bus_label, plot_format)))

                    if force_show is True:
                        plt.show()
                    elif len(busses) >= 5:
                        pass
                    else:
                        plt.show()

                    plt.close()
                except TypeError as e:
                    print("Hub.plot(): %s" % e)
            except KeyError:
                print("Hub.plot(): No output stream")

    def copy(self):
        return copy.deepcopy(self)

    def __deepcopy__(self, memo):
        """
        Copies the Hub object and all of its components.

        Note
        ----
        This function not yet uses the actual attributes for the initialisation of copies of
        components in a hub object, but rather the initialisation parameters of each component.
        This could be solved in the future introducing a .attribute method that provides up-to-date
        access to attributes of each component.

        :param memo: Parameter for the copy.deepcopy function
        :return: A copy of the Hub object and copies of all of its components
        """

        # Copy 'self._parameters' in order to initialize a new hub with same arguments,
        # use deepcopy in order to be able to pop the label string from the dictionary
        # without affecting other copy calls afterwards
        kwargs = copy.deepcopy(self._parameters)
        new_hub = Hub(label=kwargs.pop('label'),
                      **kwargs)

        # Create two lists: one for all busses and one for all other components in the hub
        busses = [component for component in self.components if isinstance(component, Bus)]
        residual = [component for component in self.components if not isinstance(component, Bus)]

        # Create a list containing new components after the loop
        # Create a dictionary with old bus objects pointing to their new counterpart
        new_components = []
        bus_mapping = {}
        for bus in busses:
            # Copy the initialization parameters of each bus and create a new counterpart
            # In the future this should be done by accessing an attribute method that contains
            # all attributes and initialization parameters to be assigned to the new component
            # TODO Introduce .attribute method for all components in order to properly copy an object
            kwargs = copy.deepcopy(bus._parameters)

            try:
                new_bus = Bus(label=kwargs.pop('label'),
                              **kwargs)
            except KeyError:
                new_bus = Bus(label=bus.label,
                              **kwargs)

            # Include old and new bus in the bus_mapping dictionary, being able to then use
            # for each old bus the new bus instead for the copied hub
            bus_mapping.update({bus: new_bus})

            # Append the newly created bus to the list of new components
            new_components.append(new_bus)

        # Loop over all other components
        for component in residual:
            # Create a dictionary containing a new stream i.e. {"inputs": {bel: Flow()}} that
            # is then used for the newly created components
            new_stream = {}

            # Loop over inputs and outputs streams of nodes (components):
            for stream in ["inputs", "outputs"]:

                # Access the old stream attributes of each component and each stream, i.e.
                # old_stream = {"inputs: {bel: Flow()}}
                # Some stream definitions are stored within the attribute 'data', some are
                # directly accessible
                try:
                    old_stream = getattr(getattr(component, stream), "data")
                except AttributeError:
                    old_stream = getattr(component, stream)

                # FIXME: Try to copy conversion factors directly using the attribute (see below)
                # Try to fetch conversion factors. Usually these should be an attribute of
                # LinearTransformer objects however are for some reason not yet properly attached.
                # Hence these factors are fetched using the .parameters dict of the Conv cls instead
                new_conversion_factors = {}
                try:
                    param_dict = getattr(component, "parameters")
                    conversion_factors = param_dict.get("conversion_factors")

                    for bus, factor in conversion_factors.items():
                        new_conversion_factors.update({bus_mapping.get(bus): factor})

                except AttributeError:
                    pass

                # Loop over "inputs" and "outputs" accumulated in old_stream dictionary
                for bus_pointer, flow_object in old_stream.items():
                    # Try to deduce which new bus shall be used instead of the old bus
                    # bus_pointer can be a WeakRef function or a plain object in storage,
                    # hence the two possibilities
                    try:
                        new_bus = bus_mapping.get(bus_pointer())
                    except TypeError:
                        new_bus = bus_mapping.get(bus_pointer)

                    # Create a new Flow object using the existing Flow object's dictionary
                    # in order to create a copy of it
                    # In case no new bus is available the issue can be found above in the
                    # creation of bus_mapping and related tasks
                    if new_bus:
                        new_flow = solph.Flow(**flow_object.__dict__)
                        new_stream.update({stream: {new_bus: new_flow}})
                    # If no bus to connect to can be found this is possibly due to that a
                    # networklink LinearTransformer is copied here. As these networklinks
                    # possibly connect to busses that are not part of the hub object itself,
                    # this will fail here. If wanted, one could implement a way to cover
                    # these cases as well. At the moment, networklinks can therefore no be
                    # copied.
                    else:
                        pass

            # Access the dictionary of parameters, ._parameters:
            kwargs = copy.deepcopy(component._parameters)
            # Remove all 'inputs', 'outputs', label', 'conversion_factors' entries in the dictionary in order to be able
            # to use it for the keyword arguments dictionary of the corresponding components copy (kwargs):
            for key in ["inp", "inputs", "out", "outputs", "label", "conversion_factors"]:
                try:
                    kwargs.pop(key)
                except KeyError:
                    pass

            # Below for each component in the hub a new component is created using the
            # previously created new_stream dictionary, i.e. a Source that delivered to the
            # Bus object 'bel' leads to a copy that will deliver to the copy of 'bel', for
            # example 'bel_2' now instead, using all parameters in the Flow object as well.
            if isinstance(component, solph.Source):
                outputs = new_stream.get("outputs")
                if outputs:
                    new_component = Conv(component.label, outputs=outputs, **kwargs)
                else:
                    new_component = None
                    print("Error in creation of new component: Source")

            elif isinstance(component, solph.LinearTransformer):
                inputs = new_stream.get("inputs")
                outputs = new_stream.get("outputs")

                # In order to initialize a new transformer object we need both inputs and outputs streams
                if inputs and outputs:
                    try:
                        new_component = Conv(component.label,
                                             inputs=inputs,
                                             outputs=outputs,
                                             conversion_factors=new_conversion_factors,
                                             **kwargs)
                    except TypeError:
                        conversion_factors = {}
                        try:
                            # In case no conversion factors have been deduced use 1 instead.
                            # This could be removed if this leads to unwanted behaviour
                            for bus, factor in new_conversion_factors.items():
                                conversion_factors.update({bus: 1})
                            print("Error in assignment of conversion factors, assumed 1 instead.")

                            new_component = Conv(component.label,
                                                 inputs=inputs,
                                                 outputs=outputs,
                                                 conversion_factors=conversion_factors,
                                                 **kwargs)
                        except TypeError:
                            new_component = None
                            print("Error in creation of new component: Linear_Transformer")
                else:
                    new_component = None

            elif isinstance(component, solph.Sink):
                inputs = new_stream.get("inputs")
                if inputs:
                    new_component = Sink(component.label, inputs=inputs, **kwargs)
                else:
                    new_component = None
                    print("Error in creation of new component: Sink")

            elif isinstance(component, solph.Storage):
                inputs = new_stream.get("inputs")
                outputs = new_stream.get("outputs")
                if inputs and outputs:
                    new_component = Store(component.label, inputs=inputs, outputs=outputs, **kwargs)
                else:
                    new_component = None
                    print("Error in creation of new component: Storage")

            else:
                new_component = None
                print("Could not find a matching class")

            # Append new component to the list of new components
            if new_component:
                new_components.append(new_component)

        # For each component in the list of new components, add the component to the newly created hub
        for component in new_components:
            new_hub.add(component)

        # Return this copy of the hub itself
        return new_hub


class Components(sortedcontainers.SortedDict):
    def __init__(self, hub_obj, components=[]):
        """
        A sortedContainers.SortedDict container allowing for integer index/ string label access of stored components in
        the container
        :param components: (list) of components that shall be assigned to the container
        """
        # Try to initialise using a list
        try:
            zipped = self.prepare_dict(components=components)

        # If this fails, one probably provided a Components instance as components input
        except AttributeError:
            components = list(components[0].values())
            zipped = self.prepare_dict(components=components)

        super().__init__(zipped)

        self.hub_obj = hub_obj

    def prepare_dict(self, components):
        labels = [component.label for component in components]
        zipped = list(zip(labels, components))
        return zipped

    def __getitem__(self, item):
        """
        Returns items stored in the .components container by subscription using indices
        or component labels.
        :param item: (int or string)
            An index or label
        :return: Object stored within the .components container
        """
        if type(item) is int:
            obj = self.get(self.iloc[item])
        else:
            obj = self.get(item)
        return obj

    def __iter__(self):
        return self.itervalues()

    def pop(self, key, default=NONE):
        """
        If *key* is in the dictionary, remove it and return its value,
        else return *default*. If *default* is not given and *key* is not in
        the dictionary, a KeyError is raised.
        """
        if key in self:
            self._list_remove(key)
            # FIXME Get to work
            # At the moment parent class of Hub (Oemof's EnergySystem) does not allow for the removal of already
            # attached components.
            self.hub_obj.align_object_containers(components=self)
            return self._pop(key)
        else:
            if default is NONE:
                raise KeyError(key)
            else:
                return default


class CopyHub(sortedcontainers.SortedDict):
    def __init__(self, Hub, factor, label=None, **kwargs):
        """
        A container for Hub or subsequent CopyHub objects. Mirrors several methods and attributes of Hub in order to
        ressemble its functionality.
        :param Hub: (obj) A Hub or CopyHub object that shall be copied using factor
        :param factor: (int) A factor for the multiplication of Hub or CopyHub objects
        :param label: (str) A label for the CopyHub object
        :param kwargs: (dict) Keyword arguments dictionary
        """
        # FIXME: Resolve deepcopy issue when copying CopyHubs. So far CopyHub's cannot have a _parameters dict,
        # otherwise copy.deepcopy will fail.
        # self._parameters = copy.deepcopy(kwargs)
        # self._parameters.update(Hub=Hub,
        #                         factor=factor,
        #                         label=label)
        self.label = Registry.register(self, CopyHub, label)

        # hub_list is used to initialize a new CopyHub object when the .__deepcopy__ method of CopyHub is called
        hub_list = kwargs.get('hub_list')
        if hub_list:
            labels = [hub.label for hub in hub_list]
            zipped = list(zip(labels, hub_list))

        else:
            hub_list = self._copy_hub(Hub, factor)
            labels = [hub.label for hub in hub_list]
            zipped = list(zip(labels, hub_list))

        super().__init__(zipped)

        self.entities = self._reassign_components()
        self._networklinks = []

        # Assign the nesting level based on the copied Hub nesting level
        try:
            if Hub.level:
                self.level = Hub.level + 1
            else:
                self.level = 1
        except AttributeError:
            pass

        self.results_df = None

    def __getitem__(self, item):
        """
        Specifies how Hub or CopyHub objects in the container can be accessed. Important also for __deepcopy__, hence
        changes should be done with care.
        :param item: (int or str) Integer index or String label in order to yield a object in the container.
        E.g. district[0] or district["House"]
        :return: Object at position or matching provided string label of the object
        """
        if type(item) is int:
            obj = self.get(self.iloc[item])
        else:
            obj = self.get(item)
        return obj

    def __iter__(self):
        """
        Specifies how to iterate over the container.
        :return: Hub or CopyHub objects in the container
        """
        for hub in self.components:
            yield hub

    def add(self, hub):
        self[hub.label] = hub

    @property
    def components(self):
        """
        Models the .components attribute
        :return: List of Hub or CopyHub objects in the container.
        """
        hub_list = [hub for hub in self.values()]
        self._components = hub_list
        return self._components

    def copy(self):
        return copy.deepcopy(self)

    @property
    def networklinks(self):
        return self._networklinks

    @networklinks.setter
    def networklinks(self, links):
        """
        Links Busses in the stored Hub based on a list of connection specifiers. A specifier can contain further
        specifiers and specifies a couple of Bus objects in a CopyHub that shall be connected using the energy system
        component class 'LinearTransformer' of Oemof. A specifier could be set up like:
        {(0, 1): (0, 0)}], this connects the first bus of the first and second Hub object in the container.
        Keys are always tuples with indices for the CopyHub.components container, while values can be of type tuple or
        of type dictionary. A value of type tuple indicates the indices of busses in the Hub.components container.
        A value of type dictionary is treated as a specifier nested within a specifier, e.g.:
        {(0, 1): {(0, 2): (0, 0)}}, this connects the first bus of the first and third Hub object in the first and
         second CopyHub object.
        :param links: (list) of dictionaries specifying connection pairs like explained above
        """
        links = [link for link in links if link.__class__ is dict]

        def unwrap(specifier, hub_1, hub_2):
            if type(list(specifier.values())[0]) is dict:
                specifier = list(specifier.values())[0]
                hubs = list(specifier.keys())[0]

                hub_1 = hub_1[hubs[0]]
                hub_2 = hub_2[hubs[1]]

                unwrap(specifier=specifier, hub_1=hub_1, hub_2=hub_2)

            elif type(list(specifier.values())[0]) is tuple:

                link_spec = list(specifier.values())[0]

                if len(link_spec) == 3:
                    conversion_factor = link_spec[1]
                    link_spec = (link_spec[0], link_spec[2])
                else:
                    conversion_factor = 1

                hub_list = [hub_1, hub_2]
                connect_to = []

                for num_spec in enumerate(link_spec):
                    hub = hub_list[num_spec[0]]
                    specifier = num_spec[1]

                    if type(specifier) is str:
                        _bus = hub.components[specifier]
                    else:
                        bus_list = []
                        for component in hub.components:
                            if isinstance(component, solph.Bus):
                                bus_list.append(component)

                        if type(specifier) is int:
                            _bus = bus_list[specifier]
                        else:
                            _bus = bus_list[0]
                            print(
                                'Networklinks: No bus specifier provided. Took the first bus in the >>components<< list')

                    connect_to.append(_bus)

                # FIXME Enable conversion factors set by sizing variable
                conversion_factors_1 = {connect_to[0]: conversion_factor,
                                        connect_to[1]: conversion_factor}
                transformer_1 = Conv(label="Networklink {0} to {1}".format(connect_to[0],
                                                                           connect_to[1]),
                                     inp={connect_to[0]: solph.Flow()},
                                     out={connect_to[1]: solph.Flow()},
                                     conversion_factors=conversion_factors_1)

                conversion_factors_2 = {connect_to[1]: conversion_factor,
                                        connect_to[0]: conversion_factor}
                transformer_1.networklinks = True
                transformer_2 = Conv(label="Networklink {0} to {1}".format(connect_to[1],
                                                                           connect_to[0]),
                                     inp={connect_to[1]: solph.Flow()},
                                     out={connect_to[0]: solph.Flow()},
                                     conversion_factors=conversion_factors_2)
                transformer_2.networklinks = True

                hub_1.add(transformer_1)
                hub_2.add(transformer_2)

                connections_spec = {'conversion_factors': [conversion_factors_1,
                                                           conversion_factors_2]}

                self._networklinks.append({(hub_1, hub_2): (connect_to[0], connect_to[1])})
                Registry.bus_connections.update({(connect_to[0], connect_to[1]): connections_spec})

        for specifier in links:
            hubs = list(specifier.keys())[0]

            hub_1 = self[hubs[0]]
            hub_2 = self[hubs[1]]

            unwrap(specifier, hub_1, hub_2)

    def optimize(self, print_output=True):
        """
        Prepares the CopyHub container for being treated as a Hub object by reassigning all components of subsequent
        CopyHub or Hub objects to a Hub object attached to the attribute .hub and calling the solver on this Hub on this
        newly created Hub object.
        :param print_output: (bool) Specifies whether the solver information shall
         be printed out
        :return: Solver information
        """
        # Create a hub object with all components of subsequent hub's in
        # the container:
        self.hub = Hub(label=self.label,
                       components=self._reassign_components(),
                       suppress_registry=True)

        output = Registry.model.run_solve(energysystem=self.hub)
        self.hub.assign_results()
        self.results_df = self.hub.results_df

        if print_output is True:
            print(output)

        return output

    def plot(self, xlabel='Date', ylabel=None, tick_distance=None, save_plot=False, force_show=False,
             plot_format='jpg'):
        """
        Plots input and output streams for each bus in the Hub. Builds upon outputlib.DataFramePlot class of Oemof.
        Uses Matplotlib and Seaborn for plotting.
        :param xlabel: (str) A label for the x axis
        :param ylabel: (str) A label for the y axis
        :param tick_distance: (int) Tick distance in hours.
        Passed to the outputlib.DataFramePlot.set_datetime_ticks method of Oemof.
        :param save_plot: (bool) Whether the plots shall be saved to the calling scripts directory
        """
        self.hub.plot(xlabel=xlabel, ylabel=ylabel, tick_distance=tick_distance, save_plot=save_plot,
                      force_show=force_show, plot_format=plot_format)

    def _copy_hub(self, Hub, n):
        """
        Copies a Hub object n times.

        Parameters
        ----------
        Hub : EnergySystem
            EnergySystem object
        n : int
            Factor for multiplying the EnergySystem
        """
        hubs = [Hub]
        for num in range(n - 1):
            hub = copy.deepcopy(Hub)
            hubs.append(hub)

        return hubs

    def _reassign_components(self):
        """
        Unwraps all energy system components of subsequent CopyHub or Hub objects and returns a list of these
        components.
        :return: List of all energy system components
        """
        hubs = []
        copy_hub_obj = self

        def unwrap_hubs(copy_hub_obj, hubs):
            if type(copy_hub_obj) is CopyHub:
                for value in copy_hub_obj.values():
                    unwrap_hubs(value, hubs)
            else:
                if type(copy_hub_obj) is Hub:
                    hubs.append(copy_hub_obj)

        unwrap_hubs(copy_hub_obj, hubs)
        components = sum([list(hub.components) for hub in hubs], [])

        return components

    def __deepcopy__(self, memo):
        """
        Specifies what shall be done when copy.deepcopy() is called on a CopyHub object
        :param memo: Parameter for the .deepcopy() function
        :return: A copy of the CopyHub object itself
        """
        hub_list = [copy.deepcopy(hub) for hub in self.components]

        return CopyHub(Hub=None, factor=None, label=self.label, hub_list=hub_list)
