from interface import Registry
import oemof.solph as solph

import copy


class Config(object):
    """
    This class holds two methods for shared tasks among different components classes.

    init_parameters:
        Mapping the shortcuts "inp" and "out" to "inputs" and "outputs" parameters of Oemof as well as
        different specifications of these parameters to the one understood by Oemof. One could say "inp=bel" to specifiy
        a Bus with label "bel" as input, and init_parameters will change this to "inputs={bel: solph.Flow()}", the format
        specification understood by Oemof. Same for e.g. "inp=[bel, bgas]", which is mapped to
        "inputs={bel: solph.Flow(), bgas: solph.Flow()}". If one wants to specify parameters for the solph.Flow object,
        he can specify e.g. "inp={bel: Flow(...)}".

    size_dispatch:
        Modifies the kwargs dictionary of a component based on the specified sizing variable.
    """

    stream_mapping = {"inp": "inputs", "out": "outputs"}

    @staticmethod
    def init_parameters(**kwargs):
        """
        Method for the adaptation of provided parameters to parameters used by Oemof components.
        Allows e,g, for specifying 'inp=bel' instead of 'inp={bel: Flow()}'. Also, parameters of
        Flow objects are supported to be specified outside of this objects parameter scope, thus enabling
        for reading components initialisation data from disc. I.e. one can specify the nominal_value of
        a Flow object for an input stream like: 'inp_nominal_value=100'.
        :param kwargs: (dict)
            Keyword dictionary as provided to components of an energy system.
        :return: Updated keyword arguments dictionary
        """

        # Gather all Flow-object relevant information (e.g. inp_nominal_value)
        flow_kwargs = {"inputs": {}, "outputs": {}}
        for column, value in kwargs.items():
            # Remove all 'nan' values from the dictionary (relevant when initializing
            # a component using i.e. ConvFromDB)
            if str(value) == "nan":
                # FIXME Enable NaN removal
                pass
                # kwargs.pop(column)
            elif "inp" in column:
                # Skip input object
                if column == "inp":
                    pass
                # Gather input flow object relevant information
                else:
                    flow_kwargs["inputs"].update({column[4:]: value})
            elif "out" in column:
                # Skip output object
                if column == "out":
                    pass
                # Gather output flow object relevant information
                else:
                    flow_kwargs["outputs"].update({column[4:]: value})

        # Replace "inp" and "out" with "inputs" and "outputs" to match Oemof parameters
        stream_mapping = Config.stream_mapping
        for stream in ["inp", "out"]:
            value = kwargs.get(stream)
            if value:
                kwargs.update({stream_mapping[stream]: value})
            try:
                kwargs.pop(stream)
            except KeyError:
                pass

        for stream in ['inputs', 'outputs']:
            specificator = kwargs.get(stream)

            if specificator:
                if type(specificator) is str:  # A case for clarification, e,g, 'inp="Radiation"'
                    kwargs.pop(stream)
                else:
                    if type(specificator) is dict:  # This is the usual case, e.g. 'inp={bus: Flow()}'
                        pass
                    elif type(specificator) is list:  # A case like 'inp=[bel, bheat]'
                        try:
                            # Initialize a new stream definition with arguments
                            specificator = {bus: solph.Flow(**flow_kwargs.get(stream)) for bus in specificator}
                        except TypeError:
                            pass
                    else:  # A case like 'inp=bel'
                        specificator = {specificator: solph.Flow(**flow_kwargs.get(stream))}
                    kwargs.update({stream: specificator})

        return kwargs

    @staticmethod
    def size_dispatch(**kwargs):
        """
        This method alters the attributes of a Flow object of a component using the parameters of the sizing variable.
        
        Parameters
        ----------

        kwargs : keyword argument dictionary
            kwargs dict containing the sizing object referenced with the key 'size'

        The method loops over streams 'inputs' and 'outputs' in the kwargs dictionary and replaces attributes of the
        flow objects of these streams with the parameters of the sizing variable.
        """
        stream_mapping = Config.stream_mapping

        size = kwargs.pop('size')
        attributes = size.attributes

        for stream in stream_mapping.keys():
            attributes.update({stream_mapping.get(stream): attributes.get(
                stream)})  # Ensure attributes dict uses 'inputs' and 'outputs' as key
            try:
                attributes.pop(stream)
            except KeyError:
                pass

        for attr, value in attributes.items():
            # Modify Flow objects only using attributes that define streams
            if attr in stream_mapping.values():
                stream = kwargs.get(attr)
                try:
                    for flow in stream.values():
                        # Replace attributes of flow objects with attributes specified with the sizing variable
                        setattr(flow, attr, value)
                except AttributeError:
                    pass

            else:
                if attr == 'label':
                    if kwargs.get('label'):
                        pass
                    else:
                        kwargs.update({attr: value})
                else:
                    kwargs.update({attr: value})
        return kwargs


class Bus(solph.Bus):
    def __init__(self, label=None, **kwargs):
        """
        The entity in an energy system connecting two components.

        :param label: (str) An identification string
        :param kwargs: Additional arguments, e.g.:

        Parameters
        ----------

        type : string
            An string describing the type of the bus
        price : float
            A price per unit
        balanced : bool
            Whether the bus is balanced
        excess : bool
            Whether the bus allows for excess flows
        """

        self._parameters = copy.deepcopy(kwargs)
        self._parameters.update(label=label)

        # kwargs mapping
        label = Registry.register(self, solph.Bus, label)
        kwargs.update({'label': label})

        # initialize super
        super().__init__(**kwargs)

        # Assigning additional attributes
        self.parameters = kwargs
        Registry.parameter_registry.update({label: self.parameters})
        self.type = kwargs.get("type")


class Conv(object):
    """
    A converter component of an energy system. Returns either a solph.Source or a solph.LinearTransformer object
    based on the input and output parameters "inp" and "out".

    :param label: (str)
        Identification string
    :param inp: (dict or list or object)
        Input parameter specification, e.g.: "inp={bel: Flow(...)}", "inp=[bel,bgas]", "inp=bel"
    :param out: (dict or list or object)
        Output parameter specification, cp. param inp
    :param kwargs: (dict)
        Additional arguments passed to the corresponding solph component class, e.g. a transformer object

    Input and output specifications can utilize the Flow class in order to specifiy flow attributes in detail:

    Parameters of the flow object
    -----------------------------
    nominal_value : numeric
        The nominal value of the flow.
    min : numeric (sequence or scalar)
        Normend minimum value of the flow. The flow absolute maximum will be
        calculatet by multiplying :attr:`nominal_value` with :attr:`min`
    max : numeric (sequence or scalar)
        Nominal maximum value of the flow. (see. :attr:`min`)
    actual_value: numeric (sequence or scalar)
        Specific value for the flow variable. Will be multiplied with the
        nominal_value to get the absolute value. If fixed is True the flow
        variable will be fixed to actual_value * nominal_value.
    positive_gradient : numeric (sequence or scalar)
        The normend maximal positive difference (flow[t-1] < flow[t])
        of two consecutive flow values.
    negative_gradient : numeric (sequence or scalar)
        The normend maximum negative difference (from[t-1] > flow[t]) of two
        consecutive timesteps.
    summed_max : numeric
        Specific maximum value summed over all timesteps. Will be multiplied
        with the nominal_value to get the absolute limit. If investment is set
        the summed_max will be multiplied with the nominal_value_variable.
    summed_min : numeric
        see above
    variable_costs : numeric (sequence or scalar)
        The costs associated with one unit of the flow.
    fixed_costs : numeric (sequence or scalar)
        The costs associated with the absolute nominal_value of the flow.
    fixed : boolean
        Boolean value indicating if a flow is fixed during the optimization
        problem to its ex-ante set value. Used in combination with the
        :attr:`actual_value`.
    investment : :class:`oemof.solph.options.Investment` object
        Object indicating if a nominal_value of the flow is determined by
        the optimization problem.
        Note: This will lead to different behaviour of attributes.

    Parameters of a transformer instance
    ------------------------------------

    conversion_factors : dict
        Dictionary containing conversion factors for conversion of inflow
        to specified outflow. Keys are output bus objects.
        The dictionary values can either be a scalar or a sequence with length
        of time horizon for simulation.

    :return: Instance of the corresponding solph component class
    """

    def __new__(cls, label=None, inp=None, out=None, **kwargs):
        _parameters = copy.deepcopy(kwargs)
        _parameters.update(label=label,
                           inp=inp,
                           out=out)

        # kwargs mapping to oemof arguments
        kwargs = Config.init_parameters(label=label,
                                        inp=inp,
                                        out=out,
                                        **kwargs)

        if kwargs.get('inputs'):
            cls = Conv.wrapper_transformer(_parameters, kwargs)
        else:
            cls = Conv.wrapper_source(_parameters, kwargs)

        return cls

    @staticmethod
    def wrapper_transformer(_parameters, kwargs):
        # kwargs mapping to oemof arguments
        kwargs["label"] = Registry.register(cls=solph.LinearTransformer, label=kwargs.get("label"))

        if kwargs.get('size'):
            kwargs = Config.size_dispatch(**kwargs)

        cls = solph.LinearTransformer(**kwargs)

        # Additional attributes
        cls.parameters = kwargs
        Registry.parameter_registry.update({kwargs["label"]: cls.parameters})
        cls._parameters = _parameters
        cls.networklinks = False
        cls.out = {bus.label: flow for (bus, flow) in cls.parameters["outputs"].items()}
        cls.out_flow = list(cls.out.values())[0]
        cls.inp = {bus.label: flow for (bus, flow) in cls.parameters["inputs"].items()}
        cls.inp_flow = list(cls.inp.values())[0]

        # FIXME Not yet implemented
        if kwargs.get("domain"):
            cls.domain = kwargs.get("domain")
        else:
            cls.domain = 'continuous'
        return cls

    @staticmethod
    def wrapper_source(_parameters, kwargs):
        # kwargs mapping to oemof arguments
        kwargs["label"] = Registry.register(cls=solph.Source, label=kwargs.get("label"))

        if kwargs.get('size'):
            kwargs = Config.size_dispatch(**kwargs)

        try:
            kwargs.pop('inputs')
        except KeyError:
            pass

        cls = solph.Source(**kwargs)

        # Additional attributes
        cls.parameters = kwargs
        Registry.parameter_registry.update({kwargs["label"]: cls.parameters})
        cls._parameters = _parameters
        cls.inp = None
        cls.inp_flow = None

        try:
            cls.out = {bus.label: flow for (bus, flow) in cls.parameters["outputs"].items()}
            cls.out_flow = list(cls.out.values())[0]
        except KeyError:
            pass

        # FIXME Not yet implemented
        if kwargs.get("domain"):
            cls.domain = kwargs.get("domain")
        else:
            cls.domain = 'continuous'
        return cls


class Sink(object):
    """
    A sink in an energy system. Returns an instance of the solph.Sink class.
    :param label: (str) Identification string
    :param eta: (int or float) Efficiency factor
    :param inp: (dict or list or object) Input parameter specification, e.g.:
    "inp={bel: Flow(...)}", "inp=[bel,bgas]", "inp=bel"

    Input and output specifications can utilize the Flow class in order to specifiy flow attributes in detail:

    Parameters
    ----------
    nominal_value : numeric
        The nominal value of the flow.
    min : numeric (sequence or scalar)
        Normend minimum value of the flow. The flow absolute maximum will be
        calculatet by multiplying :attr:`nominal_value` with :attr:`min`
    max : numeric (sequence or scalar)
        Nominal maximum value of the flow. (see. :attr:`min`)
    actual_value: numeric (sequence or scalar)
        Specific value for the flow variable. Will be multiplied with the
        nominal_value to get the absolute value. If fixed is True the flow
        variable will be fixed to actual_value * nominal_value.
    positive_gradient : numeric (sequence or scalar)
        The normend maximal positive difference (flow[t-1] < flow[t])
        of two consecutive flow values.
    negative_gradient : numeric (sequence or scalar)
        The normend maximum negative difference (from[t-1] > flow[t]) of two
        consecutive timesteps.
    summed_max : numeric
        Specific maximum value summed over all timesteps. Will be multiplied
        with the nominal_value to get the absolute limit. If investment is set
        the summed_max will be multiplied with the nominal_value_variable.
    summed_min : numeric
        see above
    variable_costs : numeric (sequence or scalar)
        The costs associated with one unit of the flow.
    fixed_costs : numeric (sequence or scalar)
        The costs associated with the absolute nominal_value of the flow.
    fixed : boolean
        Boolean value indicating if a flow is fixed during the optimization
        problem to its ex-ante set value. Used in combination with the
        :attr:`actual_value`.
    investment : :class:`oemof.solph.options.Investment` object
        Object indicating if a nominal_value of the flow is determined by
        the optimization problem.
        Note: This will lead to different behaviour of attributes.

    :param kwargs: Additional arguments passed to the solph.Sink class
    :return: Instance of the solph.Sink class
    """

    def __new__(cls, label=None, inp=None, **kwargs):
        _parameters = copy.deepcopy(kwargs)
        _parameters.update(label=label,
                           inp=inp)

        # kwargs mapping to oemof arguments
        kwargs = Config.init_parameters(label=label,
                                        inp=inp,
                                        **kwargs)

        cls = Sink.wrapper_sink(_parameters, kwargs)

        return cls

    @staticmethod
    def wrapper_sink(_parameters, kwargs):
        kwargs["label"] = Registry.register(cls=solph.Sink, label=kwargs.get("label"))

        if kwargs.get('size'):
            kwargs = Config.size_dispatch(**kwargs)

        cls = solph.Sink(**kwargs)

        # Additional attributes
        cls.parameters = kwargs
        Registry.parameter_registry.update({kwargs["label"]: cls.parameters})
        cls._parameters = _parameters
        cls.inp = {bus.label: flow for (bus, flow) in cls.parameters["inputs"].items()}
        cls.inp_flow = list(cls.inp.values())[0]
        cls.out = None
        cls.out_flow = None

        # FIXME Not yet implemented
        if kwargs.get("domain"):
            cls.domain = kwargs.get("domain")
        else:
            cls.domain = 'continuous'
        return cls


class Store(object):
    """
    A storage in an energy system. Returns an instance of the solph.Storage class.
    :param label: (str) Identification string;
    :param eta_in: (int or float) Efficiency factor of the input flow;
    :param eta_out: (int or float) Efficiency factor of the output flow;
    :param inp: (dict or list or object) Input parameter specification, e.g.:
    "inp={bel: Flow(...)}", "inp=[bel,bgas]", "inp=bel";
    :param out: (dict r list or object) Output parameter specification, cp. param inp;

    Input and output specifications can utilize the Flow class in order to specifiy flow attributes in detail:

    Parameters
    ----------
    nominal_value : numeric
        The nominal value of the flow.
    min : numeric (sequence or scalar)
        Normend minimum value of the flow. The flow absolute maximum will be
        calculatet by multiplying :attr:`nominal_value` with :attr:`min`
    max : numeric (sequence or scalar)
        Nominal maximum value of the flow. (see. :attr:`min`)
    actual_value: numeric (sequence or scalar)
        Specific value for the flow variable. Will be multiplied with the
        nominal_value to get the absolute value. If fixed is True the flow
        variable will be fixed to actual_value * nominal_value.
    positive_gradient : numeric (sequence or scalar)
        The normend maximal positive difference (flow[t-1] < flow[t])
        of two consecutive flow values.
    negative_gradient : numeric (sequence or scalar)
        The normend maximum negative difference (from[t-1] > flow[t]) of two
        consecutive timesteps.
    summed_max : numeric
        Specific maximum value summed over all timesteps. Will be multiplied
        with the nominal_value to get the absolute limit. If investment is set
        the summed_max will be multiplied with the nominal_value_variable.
    summed_min : numeric
        see above
    variable_costs : numeric (sequence or scalar)
        The costs associated with one unit of the flow.
    fixed_costs : numeric (sequence or scalar)
        The costs associated with the absolute nominal_value of the flow.
    fixed : boolean
        Boolean value indicating if a flow is fixed during the optimization
        problem to its ex-ante set value. Used in combination with the
        :attr:`actual_value`.
    investment : :class:`oemof.solph.options.Investment` object
        Object indicating if a nominal_value of the flow is determined by
        the optimization problem.
        Note: This will lead to different behaviour of attributes.

    :param kwargs: Additional arguments passed to the solph.Storage class:

    Parameters
    ----------
    nominal_capacity : numeric
        Absolute nominal capacity of the storage
    nominal_input_capacity_ratio :  numeric
        Ratio between the nominal inflow of the storage and its capacity.
    nominal_output_capacity_ratio : numeric
        Ratio between the nominal outflow of the storage and its capacity.
        Note: This ratio is used to create the Flow object for the outflow
        and set its nominal value of the storage in the constructor.
    nominal_input_capacity_ratio : numeric
        see: nominal_output_capacity_ratio
    initial_capacity : numeric
        The capacity of the storage in the first (and last) timestep of
        optimization.
    capacity_loss : numeric (sequence or scalar)
        The relative loss of the storage capacity from between two consecutive
        timesteps.
    inflow_conversion_factor : numeric (sequence or scalar)
        The relative conversion factor, i.e. efficiency associated with the
        inflow of the storage.
    outflow_conversion_factor : numeric (sequence or scalar)
        see: inflow_conversion_factor
    capacity_min : numeric (sequence or scalar)
        The nominal minimum capacity of the storage as fraction of the
        nominal capacity (between 0 and 1, default: 0).
        To set different values in every timestep use a sequence.
    capacity_max : numeric (sequence or scalar)
        see: capacity_min
    (For more information cp. Parent class)

    :return: An instance of the solph.Storage class;
    """

    def __new__(cls, label=None, inp=None, out=None, **kwargs):

        _parameters = copy.deepcopy(kwargs)
        _parameters.update(label=label,
                           inp=inp,
                           out=out)

        # kwargs mapping to oemof arguments
        kwargs = Config.init_parameters(label=label,
                                        inp=inp,
                                        out=out,
                                        **kwargs)

        cls = Store.wrapper_store(_parameters, kwargs)

        return cls

    @staticmethod
    def wrapper_store(_parameters, kwargs):
        kwargs["label"] = Registry.register(cls=solph.Storage, label=kwargs.get("label"))

        if kwargs.get('size'):
            kwargs = Config.size_dispatch(**kwargs)

        cls = solph.Storage(**kwargs)

        # Additional attributes
        cls.parameters = kwargs
        Registry.parameter_registry.update({kwargs["label"]: cls.parameters})
        cls._parameters = _parameters
        cls.out = {bus.label: flow for (bus, flow) in cls.parameters["outputs"].items()}
        cls.out_flow = list(cls.out.values())[0]
        cls.inp = {bus.label: flow for (bus, flow) in cls.parameters["inputs"].items()}
        cls.inp_flow = list(cls.inp.values())[0]

        # FIXME Not yet implemented
        if kwargs.get("domain"):
            cls.domain = kwargs.get("domain")
        else:
            cls.domain = 'continuous'
        return cls
