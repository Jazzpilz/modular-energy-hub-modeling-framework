from interface import Registry

import oemof.solph as solph

# from collections import UserDict, UserList
# from itertools import groupby

import pyomo.core as po


class CustomModel(solph.CustomModel, object):
    horizon = None
    optimize_counter = 0

    def __init__(self, horizon, solver="glpk", solve_kwargs={}, **kwargs):
        """Docstring taken partly from the parent class, 'solph.CustomModel'.

        An energy system model for operational simulation with optimized
        dispatch.

        **The following sets are created:**

        NODES
            A set with all nodes of the given energy system.
        TIMESTEPS
            A set with all time steps of the given time horizon.

        Parameters
        ----------
        horizon : pandas DatetimeIndex
            The timeindex will be used to calculate the timesteps the timeincrement
            for the optimization model
        solver : string
            A string identifying the solver to be used. E.g. 'cbc'
        solve_kwargs : dictionary
            A dictionary with solver parameters, e.g. 'tee': True

        Parameters of the parent class (to be accessed by providing them through 'kwargs'):

        es : EnergySystem object
            Object that holds the nodes of an oemof energy system graph
        constraint_groups : list
            Solph looks for these groups in the given energy system and uses them
            to create the constraints of the optimization problem.
            Defaults to :const:`OperationalModel.CONSTRAINTS`
        timesteps : sequence (optional)
            Timesteps used in the optimization model. If provided as list or
            pandas.DatetimeIndex the sequence will be used to index the timedepent
            variables, constraints etc. If not provided we will try to compute
            this sequence from attr:`timeindex`.
        timeincrement : float (optional)
            Timeincrement used in constraints and objective expressions.


        **The following sets are created:**

        NODES :
            A set with all oemof nodes.

        TIMESTEPS :
            A set with all timesteps for the optimization problem.

        FLOWS :
            A 2 dimensional set with all flows. Index: `(source, target)`

        NEGATIVE_GRADIENT_FLOWS :
            A subset of set FLOWS with all flows where attribute
            `negative_gradient` is set.

        POSITIVE_GRADIENT_FLOWS :
            A subset of set FLOWS with all flows where attribute
            `positive_gradient` is set.

        **The following variables are created:**

        flow
            Flow from source to target indexed by FLOWS, TIMESTEPS.
            Note: Bounds of this variable are set depending on attributes of
            the corresponding flow object.

        negative_flow_gradient :
            Difference of a flow in consecutive timesteps if flow is reduced
            indexed by NEGATIVE_GRADIENT_FLOWS, TIMESTEPS.

        positive_flow_gradient :
            Difference of a flow in consecutive timesteps if flow is increased
            indexed by NEGATIVE_GRADIENT_FLOWS, TIMESTEPS.

        """
        self.horizon = horizon
        CustomModel.horizon = horizon

        self.solver = solver
        self.solver_kwargs = solve_kwargs

        super().__init__(timeindex=horizon, **kwargs)

        Registry.model = self
        self.parameters = {'horizon': horizon,
                           'solver': solver,
                           'solve_kwargs': solve_kwargs,
                           'kwargs': kwargs}

    # TODO test
    def assign_loads(self, load_dict, attr_label):
        setattr(self, attr_label, po.Param(self.timeindex, initialize=load_dict))

    # TODO implement
    def assign_costs(self, cost_dict, attr_label):
        pass

    def prepare(self, energysystem):
        self.initialize_nodes_flows(energysystem=energysystem)

    def set_objective(self, sense, update=False):
        if sense == 'minimize':
            sense = po.minimize
        elif sense == 'maximize':
            sense == po.maximize
        else:
            sense = None

        self.objective_function(sense=sense, update=update)

    def run_solve(self, energysystem):
        CustomModel.optimize_counter += 1

        solph.models.CustomModel.CUSTOM_CONSTRAINTS = Add_Constraint.constraints
        self.assemble_constraints()

        # This is the normal case
        try:
            results = self.solve(energysystem=energysystem,
                                 solver=self.solver,
                                 solve_kwargs=self.solver_kwargs)
        # If it fails, then most probably because there is a second solver run ongoing.
        # In this case initialise a new model.
        except RuntimeError:
            parameters = self.parameters
            horizon = parameters.get('horizon')
            solver = parameters.get('solver')
            solve_kwargs = parameters.get('solve_kwargs')
            kwargs = parameters.get('kwargs')
            self = CustomModel(horizon=horizon,
                               solver=solver,
                               solve_kwargs=solve_kwargs,
                               **kwargs)
            results = self.solve(energysystem=energysystem,
                                 solver=self.solver,
                                 solve_kwargs=self.solver_kwargs)
        return results


class OperationalModel(solph.OperationalModel, object):
    horizon = None
    optimize_counter = 0

    def __init__(self, horizon, solver="glpk", solve_kwargs={}, **kwargs):
        """Docstring taken partly from the parent class, 'solph.CustomModel'.

        An energy system model for operational simulation with optimized
        dispatch.

        **The following sets are created:**

        NODES
            A set with all nodes of the given energy system.
        TIMESTEPS
            A set with all time steps of the given time horizon.

        Parameters
        ----------
        horizon : pandas DatetimeIndex
            The timeindex will be used to calculate the timesteps the timeincrement
            for the optimization model
        solver : string
            A string identifying the solver to be used. E.g. 'cbc'
        solve_kwargs : dictionary
            A dictionary with solver parameters, e.g. 'tee': True

        Parameters of the parent class (to be accessed by providing them through 'kwargs'):

        es : EnergySystem object
            Object that holds the nodes of an oemof energy system graph
        constraint_groups : list
            Solph looks for these groups in the given energy system and uses them
            to create the constraints of the optimization problem.
            Defaults to :const:`OperationalModel.CONSTRAINTS`
        timesteps : sequence (optional)
            Timesteps used in the optimization model. If provided as list or
            pandas.DatetimeIndex the sequence will be used to index the timedepent
            variables, constraints etc. If not provided we will try to compute
            this sequence from attr:`timeindex`.
        timeincrement : float (optional)
            Timeincrement used in constraints and objective expressions.


        **The following sets are created:**

        NODES :
            A set with all oemof nodes.

        TIMESTEPS :
            A set with all timesteps for the optimization problem.

        FLOWS :
            A 2 dimensional set with all flows. Index: `(source, target)`

        NEGATIVE_GRADIENT_FLOWS :
            A subset of set FLOWS with all flows where attribute
            `negative_gradient` is set.

        POSITIVE_GRADIENT_FLOWS :
            A subset of set FLOWS with all flows where attribute
            `positive_gradient` is set.

        **The following variables are created:**

        flow
            Flow from source to target indexed by FLOWS, TIMESTEPS.
            Note: Bounds of this variable are set depending on attributes of
            the corresponding flow object.

        negative_flow_gradient :
            Difference of a flow in consecutive timesteps if flow is reduced
            indexed by NEGATIVE_GRADIENT_FLOWS, TIMESTEPS.

        positive_flow_gradient :
            Difference of a flow in consecutive timesteps if flow is increased
            indexed by NEGATIVE_GRADIENT_FLOWS, TIMESTEPS.

        """
        self.horizon = horizon
        CustomModel.horizon = horizon

        self.solver = solver
        self.solve_kwargs = solve_kwargs

        Registry.model = self
        self.parameters = {'horizon': horizon,
                           'solver': solver,
                           'solve_kwargs': solve_kwargs,
                           'kwargs': kwargs}

    # TODO test
    def assign_loads(self, load_dict, attr_label):
        setattr(self, attr_label, po.Param(self.timeindex, initialize=load_dict))

    # TODO implement
    def assign_costs(self, cost_dict, attr_label):
        pass

    def set_objective(self, sense, update=False):
        if sense == 'minimize':
            sense = po.minimize
        elif sense == 'maximize':
            sense == po.maximize
        else:
            sense = None

        self.objective_function(sense=sense, update=update)

    def run_solve(self, energysystem):

        # This is the normal case
        try:
            super().__init__(es=energysystem,
                             timeindex=self.horizon, **self.solve_kwargs)
            CustomModel.optimize_counter += 1
            results = self.solve(energysystem=energysystem,
                                 solver=self.solver,
                                 solve_kwargs=self.solve_kwargs)
        # If it fails, then most probably because there is a second solver run ongoing.
        # In this case initialise a new model.
        except RuntimeError:
            super().__init__(es=energysystem,
                             timeindex=self.horizon, **self.solve_kwargs)
            CustomModel.optimize_counter += 1
            parameters = self.parameters
            horizon = parameters.get('horizon')
            solver = parameters.get('solver')
            solve_kwargs = parameters.get('solve_kwargs')
            kwargs = parameters.get('kwargs')
            self = CustomModel(horizon=horizon,
                               solver=solver,
                               solve_kwargs=solve_kwargs,
                               **kwargs)
            results = self.solve(energysystem=energysystem,
                                 solver=self.solver,
                                 solve_kwargs=self.solve_kwargs)
        return results


# TODO Implement and test
class Add_Constraint(po.Constraint, object):
    """
    This class is not yet properly implemented or tested. It could however start as an entry
    point for adding additional constraints. It should facilitate the process of adding
    additional constraints on top of an existing model.
    """
    constraints = []

    def __init__(self, model, rule):
        super().__init__(model.TIMESTEPS,
                         rule=rule)
        Add_Constraint.constraints.append(self)
        solph.models.CustomModel.CUSTOM_CONSTRAINTS.append(rule)
