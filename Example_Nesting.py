"""
This example shall illustrate the nesting capabilities of the framework
Example file is based on storage_invest.py covered in Oemof examples folder

Notes
-----
- At the moment .optimize() can only be called once in the model

@author: Frederik Banis
"""

import os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from interface import outputlib, logger, helpers, logging, solph, Flow, Registry
from interface.hub import Hub, CopyHub
from interface.components import Bus, Conv, Sink, Store
from interface.model import CustomModel
from interface.miscellaneous import show_tree


def initialize_model(full_timerange):
    # ##########################################################################
    # # Initialize components of hub House
    # ##########################################################################
    logger.define_logging(basicpath=os.getcwd())
    logging.info('Initialize the energy system')

    if full_timerange is True:
        periods = 8760
        filename = 'storage_invest_long.csv'
    else:
        periods = 168
        filename = 'storage_invest.csv'

    CustomModel(horizon=pd.date_range('1/1/2012', periods=periods, freq='H'),
                solver="glpk", tee=True)

    # Create energy system
    hub = Hub('Hub')

    # Read data file
    csv_file = os.path.join(os.getcwd(), 'data', filename)
    data = pd.read_csv(csv_file, sep=',')

    ##########################################################################
    # Create oemof objects
    ##########################################################################

    logging.info('Create oemof objects')

    bgas = Bus(label='natural_gas', type="gas")

    bel = Bus(label='electricity', type="el")

    # create excess component for the electricity bus to allow overproduction
    Sink(label="excess_bel",
         inp=bel)

    # create commodity object for gas resource
    Conv(label='rgas',
         out={bgas: Flow(
             nominal_value=194397000, summed_max=1)})

    # create fixed source object for wind
    Conv(label='wind',
         inp='wind',
         out={bel: Flow(actual_value=data["wind"],
                        nominal_value=1000000,
                        fixed=True,
                        fixed_costs=20)})

    # create fixed source object for pv
    Conv(label='pv',
         out={bel: Flow(actual_value=data['pv'],
                        nominal_value=582000,
                        fixed=True,
                        fixed_costs=15)})

    # create simple sink object for demand
    Sink(label='demand',
         inp={bel: Flow(actual_value=data['demand_el'],
                        fixed=True,
                        nominal_value=1)})

    # create simple transformer object for gas powerplant
    Conv(label="pp_gas",
         inp=bgas,
         out={bel: Flow(nominal_value=10e10,
                        variable_costs=50)},
         conversion_factors={bel: 0.58})

    # Calculate ep_costs from capex to compare with old solph
    capex = 1000
    lifetime = 20
    wacc = 0.05
    epc = capex * (wacc * (1 + wacc) ** lifetime) / ((1 + wacc) ** lifetime - 1)

    Store(label='storage',
          inp={bel: Flow(variable_costs=10e10)},
          out={bel: Flow(variable_costs=10e10)},
          capacity_loss=0.00, initial_capacity=0,
          nominal_input_capacity_ratio=1 / 6,
          nominal_output_capacity_ratio=1 / 6,
          inflow_conversion_factor=1, outflow_conversion_factor=0.8,
          fixed_costs=35,
          investment=solph.Investment(ep_costs=epc))
    return hub


# ##########################################################################
# Create the energy system using components of the framework Hub, Conv, Sink, Store
# ##########################################################################

hub = initialize_model(full_timerange=False)
for component in hub.components:
    print(component.label)

# ##########################################################################
# Work on and with the created energy system 'hub'.
# The .optimize method can at the moment be only called once in the CustomModel
# ##########################################################################

# hub.optimize(print_output=False)
# hub.plot(ylabel="W")

# Create array of n hubs referenced as district ############################
district = CopyHub(hub, 2, label="District")

print(district[0])
print(district['Hub_1'])

for component in district.components:
    print(component.label)

# Networklinks can be defined like: ########################################
# Connect hubs 0 and 1 using busses 'electricity' and 'electricity_2' in the
# corresponding hub.components list. Instead of providing integer indices for
# gathering the right hub, one can use string labels as well, same for specifying
# the busses. Compare city.networklinks below as well.
# Here, we use a conversion factor of 0.9 (equals eta=0.9) for the link:
district.networklinks = [{(0, 1): ('electricity', 0.9, 'electricity_1')}]
#
# # Plot the current model structure #######################################
# ------------- Create an array of n districts referenced as city
# Create an array of n districts referenced as city ########################
city = CopyHub(district, 2, label="City")
print(city.level)

city.networklinks = [{("District", "District_1"): {(1, 0): (0, 0.9, 0)}}]
print(city.networklinks)

# Add a new hub object to the 'city' object ################################
# We can specify the input of this demand using the Registry.registry
# dictionary, in which all components are registered with their labels.
# Registry.registry["electricity"] is the "electricity" bus specified in
# the initialize function above.
factory = Hub("Factory")
machine_room_1 = Sink("Machine room 1",
                      inp={Registry.registry.get('electricity'): Flow(actual_value=10e3,
                                                                      fixed=True,
                                                                      nominal_value=1)})

# You can add a hub by either using the sortedDict functionality as for a
# dictionary or the .add method of CopyHub:
# city["Factory"] = factory
city.add(factory)

# Or we might want to alter the attributes of a flow of another pv object
# in the first district and the first hub.
#  We can directly access stored objects in the hub using a subscription, as we do for
# accessing subsequent CopyHub or Hub instances in the city container.
# By decreasing the power of these two components, the previously connected hub number
# two with its electrical bus 'electricity_2' will need to cover the demand of the first
# hub with the additional machine room demand as well.
pv = city[0][0]['pv']
pv.out_flow.nominal_value = 90000
wind = city[0][0]['wind']
wind.out_flow.nominal_value = 100000

# Plot the current model structure, show also connections ##################
city.optimize()
# city.plot(save_plot=True, force_show=True, ylabel="W", plot_format='pdf')

# print the results dataframe and store it as well #########################
results = city.results_df
results.to_csv(os.path.join(os.getcwd(), 'data', 'Example_Nesting_results.csv'))
print(results)

# print the result dataframe for the 'pv' object ###########################
print(city.hub.components['pv'].output.head())


results = city.results_df
print(results)

networklink_1 = results.slice_by(obj_label="Networklink electricity_1 to electricity", type="to_bus")
pp_gas = results.slice_by(obj_label="pp_gas", type="to_bus")
pv = results.slice_by(obj_label="pv", type="to_bus")
wind = results.slice_by(obj_label="wind", type="to_bus")
demand = results.slice_by(obj_label="demand", type="from_bus")
machine_room_1 = results.slice_by(obj_label="Machine room 1", type="from_bus")

results = [networklink_1, pp_gas, pv, wind, demand, machine_room_1]
labels = ["Networklink electricity_1 to electricity", "pp_gas", "pv", "wind", "demand", "machine_room_1"]

for result, label in zip(results, labels):
    result.index = result.index.droplevel().droplevel().droplevel()
    result.columns = [label]
results = pd.concat(results, axis=1)
print(results)

sns.set_style("whitegrid")
sns.set_context("paper", rc={"grid.linewidth": .2})
sns.set_palette("hls")

plot = results.plot(style=["-", "-", "-", "-", "--", "--"], linewidth=2)

plot.set_xlabel("Time")
plot.set_ylabel("W")
plot.set_title("Hub_1")
sns.despine()
plt.tight_layout()
plt.savefig(os.path.join(os.getcwd(), "plot", "Example_Nesting.pdf"))
plt.show()

# Use the show_tree function to plot the hub tree structure (requires Graphviz to
# be installed and within 'PATH' of the OS:
show_tree(filename='city_structure', show_connections=True)
